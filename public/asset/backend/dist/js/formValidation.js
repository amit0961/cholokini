$("#passwordValidate").validate({
  rules: {
    currentPassword: {
      required: true,
      minlength:6,
      maxlength:20
    },
    newPassword: {
      required: true,
      minlength:6,
      maxlength:20
    },
    confirmPassword: {
      required: true,
      minlength:6,
      maxlength:20
    },
  },
  messages: {
    currentPassword: {
      required: "Please enter password",
    },
    newPassword: {
      required: "Please enter password",
    },
    confirmPassword: {
      required: "Please enter password",
    },
  },
});
