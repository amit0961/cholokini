<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

//************Default System  ************
//Route::get('/', function () {
//    return view('welcome');
//});
//************Default System  ************

Route::get('/','IndexController@index');
Route::match(['get','post'] ,'/admin','AdminController@login');
Route::get('/logout','AdminController@logout');
//aboutPage
Route::get('/about','IndexController@about');
//contactPage
Route::get('/contact','IndexController@contact');
//listingPage
Route::get('/products/{url}','ProductController@products');
//ProductDetailsPage
Route::get('/product/{id}', 'ProductController@productDetails');
//getProduct Attribute price url with theme.js
Route::get('/get-product-price','ProductController@getProductPrice');

//                 ************ CART SECTION ****************
//AddToCart
Route::match(['get', 'post'],'/addToCart', 'ProductController@addToCart' );
//Cart Route
Route::match(['get', 'post'],'/cart', 'ProductController@cart' );
//Delete Cart Product
Route::get('/cart/deleteCartProduct/{id}', 'ProductController@deleteCartProduct' );
//update quantity by ADD or SUB in cart
Route::get('/cart/updateCartQuantity/{id}/{quantity}','ProductController@updateCartQuantity');
//Apply Coupon in Frontend\
Route::post('/cart/applyCoupon','ProductController@applyCoupon');
//                 ************ CART SECTION ****************
//login with Google
Route::get('login/google', 'UserController@redirectToProvider');
Route::get('login/google/callback', 'UserController@handleProviderCallback');
// Users loginRegister
Route::get( '/loginRegister', 'UserController@loginRegister' );
//register page from submit
Route::post('/userRegister','UserController@register');
//login from submit
Route::post('/userLogin','UserController@login');
//logout
Route::get('/logout','UserController@logout');
//checkEmailValidation
Route::match(['get','post'] ,'/checkemail','UserController@checkemail');
//all routes after loginue
Route::group([ 'middleware' => ['frontLogin']], function() {
    //UserAccount Page Settings
    Route::match(['get','post'],'/userAccount','UserController@userAccount');
    Route::match(['get','post'],'/userProfile','UserController@userProfile');
    Route::match(['get','post'],'/userPassword','UserController@userPassword');
    // Check User Current Password
    Route::post('/check-user-pwd','UserController@chkUserPassword');
    // Update User Password
    Route::post('/updateUserPassword','UserController@updatePassword');
    Route::match(['get','post'],'/userPayment','UserController@userPayment');
});


Route::group(['middleware' => ['auth']], function () {
    Route::get('/admin/dashboard','AdminController@dashboard');
    Route::get('/admin/settings','AdminController@settings');
    Route::get('/admin/checkPassword','AdminController@checkPassword');
    Route::post('/admin/updatePassword','AdminController@updatePassword');

    //********* For Categories ********
    Route::match(['get','post'] ,'/admin/addCategory','CategoryController@addCategory');
    Route::match(['get','post'] ,'/admin/viewCategory','CategoryController@viewCategory');
    Route::match(['get','post'] ,'/admin/editCategory/{id}','CategoryController@editCategory');
    Route::match(['get','post'] ,'/admin/deleteCategory/{id}','CategoryController@deleteCategory');

    //********* For Categories ********
    Route::match(['get','post'] ,'/admin/addProduct','ProductController@addProduct');
    Route::match(['get','post'] ,'/admin/viewProduct','ProductController@viewProduct');
    Route::match(['get','post'] ,'/admin/editProduct/{id}','ProductController@editProduct');
    Route::match(['get','post'] ,'/admin/delete-product-image/{id}','ProductController@deleteProductImage');
    Route::match(['get','post'] ,'/admin/deleteProduct/{id}','ProductController@deleteProduct');
    //********* For Product Attributes ********
    Route::match(['get', 'post'], 'admin/addAttribute/{id}', 'ProductController@addAttribute');
    Route::match(['get', 'post'], 'admin/editAttribute/{id}', 'ProductController@editAttribute' );
    Route::match(['get', 'post'], 'admin/deleteAttribute/{id}', 'ProductController@deleteAttribute' );
    //********* For Product Images ********
    Route::match(['get', 'post'], 'admin/addProductImages/{id}', 'ProductController@addImages');
    Route::match(['get', 'post'], 'admin/deleteProductImage/{id}', 'ProductController@deleteProImage' );
    //**********  For coupon section  ********
    Route::match(['get', 'post'],'admin/addCoupon', 'CouponController@addCoupon');
    Route::get('admin/viewCoupon','CouponController@viewCoupon');
    Route::match(['get', 'post'], 'admin/editCoupon/{id}', 'CouponController@editCoupon' );
    Route::get('admin/deleteCoupon/{id}', 'CouponController@deleteCoupon' );


});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
