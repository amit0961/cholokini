<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class AdminController extends Controller
{
    public function login(Request $request){
        if ($request->isMethod('post')){
            $data = $request->input();
            if (Auth::attempt([
                'email'=>$data['email'],
                'password'=>$data['password'],
                'admin'=> '1'
            ])){
//                echo 'success'; die;
                return redirect('/admin/dashboard');
            }else{
                return redirect('/admin')->with('error', 'Opps..! Invalid Username or Password.');
            }
        }
        return view('backend.adminLogin');
    }
    public function dashboard(){
        return view('backend.dashboard');
    }
    public function logout(){
        Session::flush();
        return redirect('/admin')->with('success','Logged Out successfully...!');
    }
    public function settings(){
        return view('backend.settings');
    }
    public function checkPassword(Request $request){
        $data = $request->all();
        $currentPassword = $data['currentPassword'];
        $checkPassword = User::where(['admin'=>'1'])->first();
        if (Hash::check($currentPassword,$checkPassword->password)){
            echo "true"; die;
        }else{
            echo "false"; die;
        }
    }
    public function updatePassword(Request $request){
        if ($request->isMethod('post')){
            $data = $request->all();
            $checkPassword = User::where(['email'=>Auth::user()->email])->first();
            $currentPassword = $data['currentPassword'];
            if (Hash::check($currentPassword,$checkPassword->password)){
                $password = bcrypt($data['newPassword']);
                User::where('id','1')->update(['password'=>$password]);
                return redirect('/admin/settings')->with('success', 'Password Updated Successfully');
            }else{
                return redirect('/admin/settings')->with('error', 'Opps..! Incorrect Current Password');
            }
        }
    }




}
