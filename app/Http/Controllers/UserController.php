<?php

namespace App\Http\Controllers;

use App\District;
use App\Division;
use App\Union;
use App\Upazila;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Laravel\Socialite\Facades\Socialite;

class UserController extends Controller
{
    public function loginRegister()
    {
       return view('user.loginRegister');
    }
    public function redirectToProvider()
    {
        return Socialite::driver('google')->redirect();
    }
    public function handleProviderCallback()
    {
        $user = Socialite::driver('google')->user();
        $authUser= User::where('email', $user->email)->first();
        if ($authUser){
            Auth::login($authUser);
            return redirect('/');
        }else{
            $user = new User ;

            $user->save();
            Auth::login($user);
            return redirect('/');


        }
    }
    public function login(Request $request){
        if($request->isMethod('post')) {
            $data = $request->all();
            if(Auth::attempt(['email'=>$data['email'],'password'=>$data['password']])){
                Session::put('frontSession',$data['email']);
                return redirect('/cart');
            }else{
                return redirect()->back()->with('error', 'Invalid Email or Password');
            }
        }
    }

    public function register(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();
            // Check if User already exists
            $usersCount = User::where('email',$data['email'])->count();
            if($usersCount>0){
                return redirect()->back()->with('error','Opps...!!! Email Has Already Been Taken .');
            }else{
                $user = new User;
                $user->name = $data['name'];
                $user->email = $data['email'];
                $user->password = bcrypt($data['password']);
                $user->save();
                if(Auth::attempt(['email'=>$data['email'],'password'=>$data['password']])){
                    Session::put('frontSession',$data['email']);
                    return redirect('/loginRegister')->with('success','Your Registration Process is complete.');
                }
            }

        }
    }
    public function logout(){
        Auth::logout();
        Session::forget('frontSession');
        return redirect('/');
    }
    public function userAccount(){

        return view('user.accountDashboard');

    }
    public function userProfile(Request $request){
        $user_id = Auth::user()->id;
        $userDetails =User::find($user_id);

        if ($request->isMethod('post')){
            $data = $request->all();
            $user =User::find($user_id);
            $user->name = $data['name'];
            $user->address1 = $data['address1'];
            $user->address2 = $data['address2'];
            $user->division = $data['division'];
            $user->district = $data['district'];
            $user->upazila = $data['upazila'];
            $user->union = $data['union'];
            $user->pincode = $data['pincode'];
            $user->mobile = $data['mobile'];
            $user->save();
            return redirect()->back()->with('success', 'Your account details has been updated successfully.');
        }
        $unions = Union::get();
        $upazilas = Upazila::get();
        $districts =District::get();
        $divisions = Division::get();
        return view('user.userProfile',compact('userDetails','unions','upazilas','districts','divisions'));

    }
    public function userPassword(){
        return view('user.userPassword');

    }
    public function chkUserPassword(Request $request){
        $data = $request->all();
        /*echo "<pre>"; print_r($data); die;*/
        $currentPassword = $data['currentPassword'];
        $user_id = Auth::User()->id;
        $checkPassword = User::where('id',$user_id)->first();
        if(Hash::check($currentPassword,$checkPassword->password)){
            echo "true"; die;
        }else{
            echo "false"; die;
        }
    }
    public function updatePassword(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();
            /*echo "<pre>"; print_r($data); die;*/
            $oldPassword = User::where('id',Auth::User()->id)->first();
            $currentPassword = $data['currentPassword'];
            if(Hash::check($currentPassword,$oldPassword->password)){
                // Update password
                $newPassword = bcrypt($data['newPassword']);
                User::where('id',Auth::User()->id)->update(['password'=>$newPassword]);
                return redirect()->back()->with('success',' Password updated successfully!');
            }else{
                return redirect()->back()->with('error','Opps...! Current Password is incorrect!');
            }
        }
    }
    public function userPayment(){
        return view('user.userPayment');

    }


}
