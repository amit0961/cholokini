<?php

namespace App\Http\Controllers;

use App\Category;
use App\Coupon;
use App\Product;
use App\ProductAttribute;
use App\ProductImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;

class ProductController extends Controller
{
    public function addProduct(Request$request)
    {
        if ($request->isMethod('post')) {
            $data = $request->all();
            if (empty($data['category_id'])) {
                return redirect()->back()->with('error', 'Under Category is Missing');
            }

            $products = new Product;
            $products->category_id = $data['category_id'];
            $products->product_name = $data['productName'];
            $products->product_code = $data['productCode'];
            $products->product_color = $data['productColor'];
            $products->description = $data['description'];
            $products->care = $data['care'];
            $products->price = $data['price'];
            //Upload Image Section
            if ($request->hasFile('image')) {
                $img = Input::file('image');
                if ($img->isValid()) {
                    $extension = $img->getClientOriginalExtension();
                    $filename = rand(111, 99999) . '.' . $extension;
                    $large_image_path = 'asset/backend/images/products/large/' . $filename;
                    $medium_image_path = 'asset/backend/images/products/medium/' . $filename;
                    $small_image_path = 'asset/backend/images/products/small/' . $filename;
                    //Resize Image
                    Image::make($img)->encode('jpg')->save($large_image_path);
                    Image::make($img)->encode('jpg')->resize(600, 600)->save($medium_image_path);
                    Image::make($img)->encode('jpg')->resize(300, 300)->save($small_image_path);
                    //store image in products table
                    $products->image = $filename;
                }
            }
            if (empty($data['status'])) {
                $status = 0;
            } else {
                $status = 1;
            }
            $products->status = $status;
            $products->save();
            return redirect('/admin/viewProduct')->with('success', 'Product has been Added Successfully');
        }
        $categories = Category::where(['parent_id' => 0])->get();
        $categories_dropdown = "<option selected disabled>Chose the Category<option>";
        foreach ($categories as $category) {
            $categories_dropdown .= "<option value='" . $category->id . "'>" . $category->name . "<option>";
            $subcategories = Category::where(['parent_id' => $category->id])->get();
            foreach ($subcategories as $subcategory) {
                $categories_dropdown .= "<option value='" . $subcategory->id . "'> &nbsp;--&nbsp; " . $subcategory->name . "<option>";
            }
        }

        return view('backend.products.addProducts', compact('categories_dropdown'));
    }
    public function viewProduct(){
        $products =Product::get();
        $products = json_decode(json_encode($products));
        foreach ($products as $key=>$val){
            $category_name = Category::where(['id'=> $val->category_id])->first();
            $products[$key]->category_name = $category_name['name'];
        }

        return view('backend.products.viewProducts',compact('products'));
    }
    public function editProduct(Request $request,$id=null)
    {
        if ($request->isMethod('post')) {
            $data = $request->all();
            if (empty($data['status'])){
                $status = 0;
            }else{
                $status=1;
            }
            //Upload Image Section
            if ($request->hasFile('image')){
                $img = Input::file('image');
                if ($img->isValid()){
                    $extension = $img->getClientOriginalExtension();
                    $fileName = rand(111,99999).'.'.$extension;
                    $large_image_path ='asset/backend/images/products/large/'.$fileName;
                    $medium_image_path ='asset/backend/images/products/medium/'.$fileName;
                    $small_image_path ='asset/backend/images/products/small/'.$fileName;
                    //Resize Image
                    Image::make($img)->encode('jpg')->save($large_image_path);
                    Image::make($img)->encode('jpg')->resize(600,600)->save($medium_image_path);
                    Image::make($img)->encode('jpg')->resize(300,300)->save($small_image_path);
                }else{
                    $fileName = $data['current_image'];
                }
            }
            Product::where(['id'=>$id])->update([
                'category_id'=>$data['category_id'],
                'product_name'=>$data['product_name'],
                'product_code'=>$data['product_code'],
                'product_color'=>$data['product_color'],
                'description'=>$data['description'],
                'care'=>$data['care'],
                'price'=>$data['price'],
                'image'=>$fileName,
                'status'=>$status
            ]);
            return redirect('/admin/viewProduct')->with('success','Product Updated Successfully');
        }
        $products = Product::where(['id'=>$id])->first();
        //dropdown starts here
        $categories =  Category::where(['parent_id'=>0])->get();
        $categories_dropdown = "<option selected disabled>Select<option>";
        foreach ($categories as $category){
            if ($category->id==$products->category_id){
                $selected = "selected";
            }else{
                $selected = "";
            }
            $categories_dropdown .="<option value='".$category->id."' ".$selected.">".$category->name."<option>";
            $subcategories =  Category::where(['parent_id'=>$category->id])->get();
            foreach ($subcategories as $subcategory){
                if ($subcategory->id==$products->category_id){
                    $selected = "selected";
                }else{
                    $selected = "";
                }
                $categories_dropdown .="<option value='".$subcategory->id."' ".$selected."> &nbsp;--&nbsp; ".$subcategory->name."<option>";
            }
        }
        //dropdown end here
        return view('backend.products.editProducts',compact('products','categories_dropdown'));

    }
    public function deleteProductImage( $id=null)
    {
        //delete image from folder
        $productImage =Product::where(['id'=>$id])->first();
        $largeImagePath ='asset/backend/images/products/large/';
        $mediumImagePath ='asset/backend/images/products/medium/';
        $smallImagePath ='asset/backend/images/products/small/';
        if (file_exists($largeImagePath.$productImage->image)){
            unlink($largeImagePath.$productImage->image);
        }
        if (file_exists($mediumImagePath.$productImage->image)){
            unlink($mediumImagePath.$productImage->image);
        }
        if (file_exists($smallImagePath.$productImage->image)){
            unlink($smallImagePath.$productImage->image);
        }
        //deleteimagefromFolder
        Product::where(['id'=>$id])->update(['image'=>'']);
        return redirect()->back()->with('success','Product Image has been Deleted Successfully');
    }
    public function deleteProduct(Request $request , $id=null)
    {
        if (!empty($id)){
            Product::where(['id'=>$id])->delete();
            return redirect('/admin/viewProduct')->with('success','Product Deleted Successfully');
        }
    }

    public function addAttribute(Request $request, $id=null)
    {
        $products = Product::with('productattribute')->where(['id'=>$id])->first();
        if ($request->isMethod('post')) {
            $data = $request->all();
            foreach ($data['sku'] as $key=>$val){
                if (!empty($val)){
                    //sku check
                    $attrCountSKU =ProductAttribute::where('sku',$val)->count();
                    if($attrCountSKU>0){
                        return redirect('admin/addAttribute/'.$id)->with('error','Opps!!! Product SKU already exist.');
                    }
                    //duplicate size prevent
                    $attrCountSize =ProductAttribute::where(['product_id'=>$id,'size'=>$data['size'][$key]])->count();
                    if($attrCountSize>0){
                        return redirect('admin/addAttribute/'.$id)->with('error', 'Opps!!! "'.$data['size'][$key].'" Size of this product already exist.');
                    }
                    //duplicateSize
                    $attribute =new ProductAttribute;
                    $attribute->product_id = $id;
                    $attribute->sku=$val;
                    $attribute->size = $data['size'][$key];
                    $attribute->price = $data['price'][$key];
                    $attribute->stock = $data['stock'][$key];
                    $attribute->save();
                }
            }
            return redirect('admin/addAttribute/'.$id)->with('success','Product Attribute has been Added Successfully');
        }
        return view('backend.attributes.addAttributes', compact('products'));
    }
    public function editAttribute(Request $request, $id=null){
        if ($request->isMethod('post')){
            $data = $request->all();
            foreach ($data['idAttr'] as $key=>$attr) {
                ProductAttribute::where(['id'=>$data['idAttr'][$key]])->update(['price'=>$data['price'][$key],'stock'=>$data['stock'][$key]]);
            }
        }
        return redirect()->back()->with('success' ,'Product Attribute has been Updated Successfully');

    }
    public function deleteAttribute(Request $request, $id=null){
        if (!empty($id)) {
            ProductAttribute::where(['id' => $id])->forceDelete();
            return redirect()->back()->with('success', 'Product Attribute has been Deleted Successfully');
        }

    }
    public function addImages(Request $request, $id=null)
    {
        $products = Product::with('productattribute')->where(['id'=>$id])->first();
        if ($request->isMethod('post')) {
            $data = $request->all();
            if ($request->hasFile('image')){
                $files = $request->file('image');
                foreach ($files as $file){
                    $image = new ProductImage;
                    $extension = $file->getClientOriginalExtension();
                    $fileName = rand(111,99999).'.'.$extension;
                    $large_image_path ='asset/backend/images/products/large/'.$fileName;
                    $medium_image_path ='asset/backend/images/products/medium/'.$fileName;
                    $small_image_path ='asset/backend/images/products/small/'.$fileName;
                    //Resize Image
                    Image::make($file)->encode('jpg')->save($large_image_path);
                    Image::make($file)->encode('jpg')->resize(600,600)->save($medium_image_path);
                    Image::make($file)->encode('jpg')->resize(300,300)->save($small_image_path);
                    $image->image =$fileName;
                    $image->product_id =$data['product_id'];
                    $image->save();
                }
            }
            return redirect('admin/addProductImages/'.$id)->with('success','Product Image Added Successfully');
        }

        $productImages = ProductImage::where(['product_id'=>$id])->get();

        return view('backend.products.addImage', compact('products','productImages'));
    }
    public function deleteProImage(Request $request, $id=null)
    {
        if (!empty($id)) {
            ProductImage::where(['id' => $id])->forceDelete();
            return redirect()->back()->with('success', 'Product Image has been Deleted Successfully');
        }

    }
    public function products($url=null){
        //show error page
        $countCategory = Category::where(['url'=>$url,'status'=>1])->count();
        if ($countCategory==0){
            abort(404);
        }
        //get all Categories and subCategories
        $categories = Category::with('categories')->where(['parent_id'=>0])->get();
        $categoryDetails = Category::where(['url'=>$url])->first();

        if($categoryDetails->parent_id==0){
            //if url is mainCategory
            $subCategories =Category::where(['parent_id'=>$categoryDetails->id])->get();
            foreach ($subCategories as $subCategory){
                $cat_ids[]= $subCategory->id;
            }
            $products =Product::whereIn('category_id',$cat_ids)->get();
        }else{
            //if url is sub category
            $products =Product::where(['category_id'=>$categoryDetails->id])->get();

        }


        return view('frontend.listing',compact('categories','products','categoryDetails'));
    }

    public function productDetails($id=null){
        $productDetails=Product::with('productattribute')->where('id',$id)->first();
        //related products
        $relatedProducts = Product::where('id','!=',$id)->where(['category_id'=>$productDetails->category_id])->get();
        //get product Alternate Images
        $proAltImages = ProductImage::where('product_id',$id)->get();
        //for Availablity stock
        $totalStock = ProductAttribute::where('product_id',$id)->sum('stock');

        return view('frontend.productDetails',compact('productDetails','relatedProducts','totalStock','proAltImages'));

    }
    public function getProductPrice(Request $request){
        $data = $request->all();
        $proArr = explode("-", $data['idSize']);
        $proArr = ProductAttribute::where(['product_id'=>$proArr[0],'size'=>$proArr[1]])->first();
        echo $proArr->price;
        echo "#";
        echo $proArr->stock;
    }
    public function addToCart(Request$request){
        Session::forget('CouponAmount');
        Session::forget('CouponCode');
        $data = $request->all();
        if (empty($data['user_email'])){
            $data['user_email']= '';
        }
        $session_id = Session::get('session_id');
        if (empty($session_id)){
            $session_id = str_random(40);
            Session::put('session_id', $session_id);
        }
        $sizeArr = explode("-", $data['size']);
        //duplicate check in cart
        $countProducts = DB::table('cart')->where([
            'product_id'=>$data['product_id'],
            'product_color'=>$data['product_color'],
            'size'=>$sizeArr[1],
            'session_id'=>$session_id
        ])->count();
        if($countProducts>0){
            return redirect()->back()->with('error', 'Opps...! Product already exist in Cart.');
        }else{
            $getsku = ProductAttribute::select('sku')->where(['product_id'=>$data['product_id'],'size'=>$sizeArr[1]])->first();
            DB::table('cart')->insert([
                'product_id'=>$data['product_id'],
                'product_name'=>$data['product_name'],
                'product_code'=>$getsku->sku,
                'product_color'=>$data['product_color'],
                'price'=>$data['price'],
                'size'=>$sizeArr[1],
                'quantity'=>$data['quantity'],
                'user_email'=>$data['user_email'],
                'session_id'=>$session_id,
            ]);
        }
        return redirect('cart')->with('success', 'Product has been added in Cart Successfully.');
    }
    public function cart(){
        $session_id = Session::get('session_id');
        $userCart = DB::table('cart')->where(['session_id'=>$session_id])->get();
        //image show in cart
        foreach ($userCart as $key=>$product) {
            $products =Product::where('id',$product->product_id)->first();
            $userCart[$key]->image = $products ->image;
        }
        return view('frontend.addToCart', compact('userCart'));
    }
    public function deleteCartProduct($id=null){
        Session::forget('CouponAmount');
        Session::forget('CouponCode');
        DB::table('cart')->where('id',$id)->delete();
        return redirect('cart')->with('success','Product has been deleted form Cart!');
    }
    public function updateCartQuantity( $id=null, $quantity=null ){
        Session::forget('CouponAmount');//that is use to prevent the coupon issue when add a product
        Session::forget('CouponCode');
        $getCartDetails = DB::table('cart')->where('id',$id)->first();
        $getAttributeStock = ProductAttribute::where('sku',$getCartDetails->product_code)->first();
        $updateQuantity = $getCartDetails->quantity+$quantity;
        //checkProductStock
        if ($getAttributeStock->stock >= $updateQuantity){
            DB::table('cart')->where('id',$id)->increment('quantity',$quantity);
            return redirect('cart')->with('success','Product Quantity has been updated successfully!');
        }else{
            return redirect('cart')->with('error','Required Product Quantity is not available!');
        }
    }
    public function applyCoupon(Request $request)
    {
        Session::forget('CouponAmount');
        Session::forget('CouponCode');
        $data = $request->all();
        $coupons = Coupon::where('coupon_code', $data['coupon_code'])->count();

        //Coupon code available check
        if ($coupons == 0) {
            return redirect('cart')->with('error', 'Opps!! Product Coupon  does not exist!');
        } else {
            //get coupon details
            $coupons = Coupon::where('coupon_code', $data['coupon_code'])->first();
            //if coupon is inactive
            if ($coupons->status == 0) {
                return redirect('cart')->with('error', 'Opps!! Product Coupon  is not active!');

            }
            //if coupon is out of expiryDate
            $expiry_date = $coupons->expiry_date;
            $current_date = date('Y-m-d');
            if ($expiry_date < $current_date) {
                return redirect('cart')->with('error', 'Opps!! Product Coupon  is expired!');
            }
            //coupon id valid for Discount
//            $session_id = Session::get('session_id');
            //get Total Amount
            $session_id = Session::get('session_id');
            $userCart = DB::table('cart')->where(['session_id' => $session_id])->get();
            $total_amount = 0;
            foreach ($userCart as $item) {
                $total_amount = $total_amount + ($item->price * $item->quantity);
            }
            //check amount fixed or percentage
            if ($coupons->amount_type == "Fixed") {
                $couponAmount = $coupons->amount;
            } else {
                $couponAmount = $total_amount * ($coupons->amount / 100);
            }
            Session::put('CouponAmount', $couponAmount);
            Session::put('CouponCode', $data['coupon_code']);
            return redirect('cart')->with('success', 'Coupon Code successfully applied. You are availing discount!');
        }

    }
}
