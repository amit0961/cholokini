<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index(){
        $products=Product::get();
        $categories = Category::with('categories')->where(['parent_id'=>0])->get();
        return view('frontend.index',compact('products','categories'));
    }
    public function about(){
        return view('frontend.about');
    }
    public function contact(){
        return view('frontend.contact');
    }
}
