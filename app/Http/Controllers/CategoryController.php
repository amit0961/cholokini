<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function addCategory(Request $request)
    {
        if ($request->isMethod('post')){
            $data =$request->all();
            if (empty($data['status'])){
                $status = 0;
            }else{
                $status=1;
            }
            $categories = new Category;
            $categories->name = $data['categoryName'];
            $categories->parent_id = $data['parent_id'];
            $categories->description = $data['description'];
            $categories->url = $data['url'];
            $categories->status =$status;
            $categories->save();
            return redirect('/admin/viewCategory')->with('success','Category Added Successfully');
        }
        $levels =  Category::where(['parent_id'=>0])->get();
        return view('backend.categories.addCategories',compact('levels'));
    }
    public function viewCategory(){
        $categories= Category::get();
        return view('backend.categories.viewCategories',compact('categories'));
    }
    public function editCategory(Request $request, $id=null){
        if ($request->isMethod('post')){
            $data = $request->all();
            if (empty($data['status'])){
                $status = 0;
            }else{
                $status=1;
            }
            Category::where(['id'=>$id])->update([
                'name'=>$data['categoryName'],
                'description'=>$data['description'],
                'url'=>$data['url'],
                'status'=>$status
            ]);
            return redirect('/admin/viewCategory')->with('success','Category Updated Successfully');
        }
        $categories = Category::where(['id'=>$id])->first();
        $levels =  Category::where(['parent_id'=>0])->get();
        return view('backend.categories.editCategories', compact('categories','levels'));

    }
    public function deleteCategory(Request $request , $id=null)
    {
        if (!empty($id)){
            Category::where(['id'=>$id])->delete();
            return redirect('/admin/viewCategory')->with('success','Category Deleted Successfully');
        }
    }





}
