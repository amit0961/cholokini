<?php

namespace App\Http\Controllers;

use App\Coupon;
use Illuminate\Http\Request;

class CouponController extends Controller
{
    public function addCoupon(Request $request){
        if ($request->isMethod('post')){
            $data = $request->all();
            if (empty($data['status'])){
                $status = 0;
            }else{
                $status=1;
            }
            $coupons= new Coupon;
            $coupons->coupon_code = $data['coupon_code'];
            $coupons->amount = $data['amount'];
            $coupons->amount_type = $data['amount_type'];
            $coupons->expiry_date = $data['expiry_date'];
            $coupons->status = $status;
            $coupons->save();
            return redirect('/admin/viewCoupon')->with('success','Coupon has been Added Successfully');
        }
        return view('backend.coupons.addCoupon');
    }
    public function viewCoupon()
    {
        $coupons = Coupon::get();
        return view('backend.coupons.viewCoupon', compact('coupons'));
    }
    public function editCoupon(Request $request,$id=null)
    {
        if ($request->isMethod('post')) {
            $data = $request->all();
            if (empty($data['status'])){
                $status = 0;
            }else{
                $status=1;
            }

            Coupon::where(['id'=>$id])->update([
                'coupon_code' => $data['coupon_code'],
                'amount' => $data['amount'],
                'amount_type' => $data['amount_type'],
                'expiry_date' => $data['expiry_date'],
                'status' => $status,
            ]);
            return redirect('/admin/viewCoupon')->with('success','Coupon Updated Successfully');

        }
        $coupons = Coupon::where(['id'=>$id])->first();
        return view('backend.coupons.editCoupon',compact('coupons'));
    }
    public function deleteCoupon($id)
    {
        if (!empty($id)){
            Coupon::where(['id'=>$id])->delete();
            return redirect('/admin/viewCoupon')->with('success','Coupon Deleted Successfully');
        }
    }




}
