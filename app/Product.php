<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function productattribute()
    {
        return $this->hasMany(ProductAttribute::class, 'product_id');
    }
}
