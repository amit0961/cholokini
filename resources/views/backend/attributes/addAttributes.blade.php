@extends('layouts.backend.backendDesign')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2 mt-3 shadow">
                    <div class="col-sm-6">
                        <h1 class="text-bold">Product-Attributes</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active text-bold">Product-Attributes</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <section class="content mt-5">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <!-- left column -->

                    <div class="col-md-10">
                        @if (Session::get('error'))
                            <div class="alert alert-danger alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{!! session('error') !!}</strong>
                            </div>
                        @endif
                        @if (Session::get('success'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{!! session('success') !!}</strong>
                            </div>
                        @endif
                        <br>

                        <!-- Horizontal Form -->
                        <div class="card card-info">
                            <div class="card-header">
                                <h3 class="card-title">Product Attributes </h3>
                            </div>

                            <!-- /.card-header -->
                            <!-- form start -->
                            <form  action="{{url('admin/addAttribute/'.$products->id)}}" enctype="multipart/form-data" class="form-horizontal" name="addProduct" id="addProduct" method="post" >
                                {{csrf_field()}}
                                <div class="card-body">
                                    <div class="form-group row ">
                                        <label for="productName" class="col-sm-4 col-form-label">Product Name</label>

                                        <div class="col-sm-8">
                                            <label for="productName" class="col-sm-4 col-form-label">{{$products->product_name}}</label>
                                        </div>
                                    </div>
                                    <div class="form-group row ">
                                        <label for="productCode" class="col-sm-4 col-form-label">Product Code</label>
                                        <div class="col-sm-8">
                                            <label for="productName" class="col-sm-4 col-form-label">{{$products->product_code}}</label>
                                        </div>
                                    </div>
                                    <div class="form-group row ">
                                        <label for="productColor" class="col-sm-4 col-form-label">Product Color</label>
                                        <div class="col-sm-8">
                                            <label for="productName" class="col-sm-4 col-form-label">{{$products->product_color}}</label>
                                        </div>
                                    </div>
                                    <div class="form-group row ">
                                        <label for="productColor" class="col-sm-4 col-form-label">Product Price</label>
                                        <div class="col-sm-8">
                                            <label for="productName" class="col-sm-4 col-form-label">{{$products->price}}</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row field_wrapper">
{{--                                    <label for="productColor" class="col-sm-4 col-form-label"></label>--}}
                                    <div class="col-sm-12 mb-3">
                                        <a href="javascript:void(0);" class="add_button" title="Add field"><i class="fa fa-plus-circle" aria-hidden="true"></i> ADD</a>

                                        <input type="text" name="sku[]" id="sku" placeholder="SKU"/>
                                        <input type="text" name="size[]" id="size" placeholder="SIZE"/>
                                        <input type="text" name="price[]" id="price" placeholder="PRICE"/>
                                        <input type="text" name="stock[]" id="stock" placeholder="STOCK"/>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                                <div class="mb-3 card-footer row justify-content-center" >
                                    <button type="submit" class="btn btn-info">Add Attribute</button>

                                </div>
                                <!-- /.card-footer -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="content mt-5">
            <div class="row">
                <div class="col-12">

                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">All the Products Attributes are here....</h3>
                        </div>
                        <!-- /.card-header -->
                        <form  action="{{url('admin/editAttribute/'.$products->id)}}" enctype="multipart/form-data" class="form-horizontal" name="editProduct" id="editProduct" method="post" >
                            {{csrf_field()}}
                            <div class="card-body">
                                <table id="example1" class="table table-hover table-bordered table-striped">
                                    <thead>
                                    <tr class="text-center">
                                        <th>Attribute ID</th>
                                        <th>SKU</th>
                                        <th>Size</th>
                                        <th>Price</th>
                                        <th>Stock</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($products['productAttribute'] as $attribute)
                                        <tr class="text-center">

                                                <td><input type="hidden" name="idAttr[]" value="{{$attribute->id}}">{{$attribute->id}}</td>
                                                <td>{{$attribute->sku}}</td>
                                                <td>{{$attribute->size}}</td>
                                                <td><input type="text" name="price[]" value="{{$attribute->price}}"></td>
                                                <td><input type="text" name="stock[]" value="{{$attribute->stock}}"></td>
                                                <td >
                                                    <input type="submit" value="Update" class="btn btn-primary btn-sm">
                                                    <a href="{{url('/admin/deleteAttribute/'. $attribute->id)}}" id="deleteCat" class="btn btn-danger btn-sm">Delete</a>
                                                </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </form>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>

    </div>

@stop

