@extends('layouts.backend.backendDesign')
@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2  mt-3 shadow">
                    <div class="col-sm-6">
                        <h1 class="text-bold" >View Product</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active text-bold">View Product</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    @if (Session::get('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{!! session('success') !!}</strong>
                        </div>
                    @endif

                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">All the Products are here....</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-hover table-bordered table-striped">
                                <thead>
                                <tr class="text-center">
                                    <th>Product ID</th>
                                    <th>Category Name</th>
                                    <th>Product Name</th>
                                    <th>Product Code</th>
                                    <th>Product Color</th>
                                    <th>Price</th>
                                    <th>Image</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($products as $product)
                                    <tr>

                                        <td>{{$product->id}}</td>
                                        <td>{{$product->category_name}}</td>
                                        <td>{{$product->product_name}}</td>
                                        <td>{{$product->product_code}}</td>
                                        <td>{{$product->product_color}}</td>
                                        <td>{{$product->price}}</td>
                                        <td>
                                            @if(!empty($product->image))
                                                <img src="{{ asset('asset/backend/images/products/small/'.$product->image) }}" style="width: 50px">
                                            @endif
                                        </td>
                                        <td class="row justify-content-center">
                                            <a href="#myModal {{$product->id}}" data-target="#myModal{{$product->id}}  " data-toggle="modal" class="btn btn-outline-success btn-sm">View</a>
                                            <a href="{{url('admin/editProduct/'.$product->id)}}" id="editCat" class="btn btn-outline-primary btn-sm">Edit</a>
                                            <a href="{{url('admin/addAttribute/'.$product->id)}}"  class="btn btn-outline-warning btn-sm">Attr.</a>
                                            <a href="{{url('admin/addProductImages/'.$product->id)}}" class="btn btn-outline-info btn-sm">Image</a>
                                            <a href="{{url('admin/deleteProduct/'.$product->id)}}" class="btn btn-outline-danger btn-sm">Delete</a>
                                        </td>
                                    </tr>

                                    <!-- Modal -->
                                    <div class="modal fade" id="myModal{{$product->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Full Details Here....</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="row">
                                                    <div class="modal-body col-md-7">
                                                        <p>Product Name = {{$product->product_name}} </p>
                                                        <p>ID:-{{$product->id}}</p>
                                                        <p>Category ID:-{{$product->category_id}}</p>
                                                        <p>Code:-{{$product->product_code}}</p>
                                                        <p>Color:{{$product->product_color}}</p>
                                                        <p>Material & Care:{{$product->care}}</p>
                                                        <p>Price: ৳ {{$product->price}}</p>
                                                        <p>Description:{{$product->description}}</p>
                                                    </div>
                                                    <div class="modal-body col-md-4 right">
                                                        <img style="width: 150px ; " src="{{asset('asset/backend/images/products/small/' .$product->image)}}" alt=" " >
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@stop
