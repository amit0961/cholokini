@extends('layouts.backend.backendDesign')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2 mt-3 shadow">
                    <div class="col-sm-6">
                        <h1 class="text-bold">Edit Products</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active text-bold">Edit Products</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <section class="content mt-5">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <!-- left column -->

                    <div class="col-md-10">
                        @if (Session::get('error'))
                            <div class="alert alert-danger alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{!! session('error') !!}</strong>
                            </div>
                        @endif
                        @if (Session::get('success'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{!! session('success') !!}</strong>
                            </div>
                        @endif
                        <br>

                        <!-- Horizontal Form -->
                        <div class="card card-info">
                            <div class="card-header">
                                <h3 class="card-title">Edit-Products</h3>
                            </div>

                            <!-- /.card-header -->
                            <!-- form start -->
                            <form action="{{url('admin/editProduct/'.$products->id)}}" enctype="multipart/form-data" class="form-horizontal" name="editProduct" id="editProduct" method="post" >
                                {{csrf_field()}}
                                <div class="card-body">
                                    <div class="form-group row">
                                        <label for="category_id" class="col-sm-4 col-form-label">Under Category</label>
                                        <div class="col-sm-8">
                                            <select name="category_id"  class="form-control" required>
                                                <?php echo $categories_dropdown ; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row ">
                                        <label for="product_name" class="col-sm-4 col-form-label">Product Name</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="product_name" id="product_name" value="{{$products->product_name}}" >
                                        </div>
                                    </div>
                                    <div class="form-group row ">
                                        <label for="product_code" class="col-sm-4 col-form-label">Product Code</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="product_code" id="product_code" value="{{$products->product_code}}" >
                                        </div>
                                    </div>
                                    <div class="form-group row ">
                                        <label for="product_color" class="col-sm-4 col-form-label">Product Color</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="product_color" id="product_color" value="{{$products->product_color}}" >
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="description" class="col-sm-4 col-form-label">Description</label>
                                        <div class="col-sm-8">
                                            <div>
                                                    <textarea name="description" id="description" class="textarea" value=""
                                                              style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{$products->description}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row ">
                                        <label for="care" class="col-sm-4 col-form-label">Material & Care</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="care" id="care" value="{{$products->care}}" >
                                        </div>
                                    </div>
                                    <div class="form-group row ">
                                        <label for="price" class="col-sm-4 col-form-label">Price</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="price" id="price" value="{{$products->price}}" >
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="image" class="col-sm-4 col-form-label">Image</label>
                                        <div class="form-group col-sm-8 ">
{{--                                            <input type="file" class="form-control-file" name="image" id="image">--}}
{{--                                            <a  href="{{url('/admin/delete-product-image/'.$products->id)}}">Delete</a>--}}
                                            <input type="file" name="image" id="image"  >
                                            <input type="hidden" name="current_image" value="{{$products->image}}"  >
                                            @if(!empty($products->image))
                                                <img style="width: 50px" src="{{asset('asset/backend/images/products/small/' .$products->image)}}" alt=" " > |
                                                <a  href="{{url('/admin/delete-product-image/'.$products->id)}}">Delete</a>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="mt-5 form-check ">
                                        <input type="checkbox" class="form-check-input" id="status" name="status" @if($products->status=="1") checked @endif value="1">
                                        <label class="form-check-label" for="exampleCheck1">Enable</label>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                                <div class="mb-3 card-footer row justify-content-center" >
                                    <button type="submit" class="btn btn-info">Update Product</button>

                                </div>
                                <!-- /.card-footer -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@stop

