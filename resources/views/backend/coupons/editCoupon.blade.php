@extends('layouts.backend.backendDesign')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2  mt-3 shadow">
                    <div class="col-sm-6">
                        <h1 class="text-bold ">Edit - Coupon</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active text-bold">Edit Coupon</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <section class="content mt-5">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <!-- left column -->

                    <div class="col-md-10">
                        @if (Session::get('error'))
                            <div class="alert alert-danger alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{!! session('error') !!}</strong>
                            </div>
                        @endif
                        @if (Session::get('success'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{!! session('success') !!}</strong>
                            </div>
                        @endif
                        <br>

                        <!-- Horizontal Form -->
                        <div class="card card-info">
                            <div class="card-header">
                                <h3 class="card-title">Edit-Coupons</h3>
                            </div>

                            <!-- /.card-header -->
                            <!-- form start -->
                            <form action="{{url('admin/editCoupon/'.$coupons->id)}}" enctype="multipart/form-data" class="form-horizontal" name="editCoupon" id="editCoupon" method="post" >
                                {{csrf_field()}}
                                <div class="card-body">
                                    <div class="form-group row ">
                                        <label for="coupon_code" class="col-sm-4 col-form-label">Coupon Code</label>
                                        <div class="col-sm-8">
                                            <input value="{{$coupons->coupon_code}}" type="text" class="form-control" name="coupon_code" id="coupon_code" placeholder="Coupon Code" >
                                        </div>
                                    </div>
                                    <div class="form-group row ">
                                        <label for="amount" class="col-sm-4 col-form-label">Amount</label>
                                        <div class="col-sm-8">
                                            <input value="{{$coupons->amount}}" type="number" class="form-control" name="amount" id="amount" min="1" placeholder="Amount" >
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="amount_type" class="col-sm-4 col-form-label">Amount Type</label>
                                        <div class="col-sm-8">
                                            <select name="amount_type" id="amount_type"  class="form-control" required >
                                                <option @if($coupons->amount_type=="Percentage") selected @endif value="Percentage">Percentage</option>
                                                <option @if($coupons->amount_type=="Fixed") selected @endif value="Fixed">Fixed</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row ">
                                        <label for="expiry_date" class="col-sm-4 col-form-label">Expiry Date</label>
                                        <div class="col-sm-8">
                                            <input value="{{$coupons->expiry_date}}" type="date" class="form-control" name="expiry_date" id="expiry_date" placeholder="Expiry Date" >
                                        </div>
                                    </div>

                                    <div class="mt-5 form-check ">
                                        <input type="checkbox" class="form-check-input" id="status" name="status" value="1" @if($coupons->status==1) checked @endif >
                                        <label class="form-check-label" for="status">Enable</label>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                                <div class="mb-3 card-footer row justify-content-center" >
                                    <button type="submit" class="btn btn-info">Update-Coupon</button>

                                </div>
                                <!-- /.card-footer -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop
