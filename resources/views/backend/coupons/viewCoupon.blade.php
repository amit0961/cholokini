@extends('layouts.backend.backendDesign')
@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2 mt-3 shadow">
                    <div class="col-sm-6">
                        <h1 class="text-bold ">View - Coupons</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active text-bold">View Coupons</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    @if (Session::get('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{!! session('success') !!}</strong>
                        </div>
                    @endif

                    <div class="card mt-5">
                        <div class="card-header">
                            <h3 class="card-title text-bold">All the Coupons are here....</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-hover table-bordered table-striped">
                                <thead>
                                <tr class="text-center">
                                    <th>Coupon ID</th>
                                    <th>Coupon Code</th>
                                    <th>Amount </th>
                                    <th>Amount Type</th>
                                    <th>Expiry Date</th>
                                    <th>Created Date</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($coupons as $coupon)
                                    <tr>

                                        <td>{{$coupon->id}}</td>
                                        <td>{{$coupon->coupon_code}}</td>
                                        <td>{{$coupon->amount}}
                                            @if($coupon->amount_type=="Percentage") % @else  ৳ @endif
                                        </td>
                                        <td>{{$coupon->amount_type}}</td>
                                        <td>{{$coupon->expiry_date}}</td>
                                        <td>{{$coupon->created_at}}</td>
                                        <td>
                                            @if($coupon->status==1) Active @else  Inactive @endif
                                        </td>
                                        <td class="row justify-content-center">
                                            <a href="{{url('admin/editCoupon/'.$coupon->id)}}" id="editCat" class="btn btn-outline-success btn-sm">Edit</a>
                                            <a href="{{url('admin/deleteCoupon/'.$coupon->id)}}" id="delCat" class="btn btn-outline-danger btn-sm">Delete</a>
                                        </td>
                                    </tr>

                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@stop
