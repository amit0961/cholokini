@extends('layouts.backend.backendDesign')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2 mt-3 shadow">
                    <div class="col-sm-6">
                        <h1 class="text-bold">Edit Categories</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active text-bold">Edit Categories</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <section class="content mt-5">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <!-- left column -->

                    <div class="col-md-10">
                        <!-- Horizontal Form -->
                        <div class="card card-info">
                            <div class="card-header">
                                <h3 class="card-title">Edit-Categories</h3>
                            </div>

                            <!-- /.card-header -->
                            <!-- form start -->
                            <form action="{{url('admin/editCategory/'.$categories->id)}}" class="form-horizontal" name="addCategory" id="addCategory" method="post" >
                                {{csrf_field()}}
                                <div class="card-body">
                                    <div class="form-group row ">
                                        <label for="categoryName" class="col-sm-4 col-form-label">Category Name</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="categoryName" id="categoryName" value="{{$categories->name}}"  >
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="parent_id" class="col-sm-4 col-form-label">Category level</label>
                                        <div class="col-sm-8">
                                            <select name="parent_id"  class="form-control" required>
                                                <option value="0">Main Category</option>
                                                @foreach($levels as $level)
                                                    <option value="{{$level->id}}" @if ($level->id==$categories->parent_id) selected @endif >{{$level->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="description" class="col-sm-4 col-form-label">Description</label>
                                        <div class="col-sm-8">
                                            <div>
                                                    <textarea name="description" id="description" class="textarea"
                                                              style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{$categories->description}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row ">
                                        <label for="url" class="col-sm-4 col-form-label">URL</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="url" id="url" value="{{$categories->url}}" >
                                        </div>
                                    </div>
                                    <div class="mt-5 form-check ">
                                        <input type="checkbox" class="form-check-input" id="status" name="status"  @if($categories->status=="1") checked @endif value="1">
                                        <label class="form-check-label" for="exampleCheck1">Enable</label>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                                <div class="mb-3 card-footer row justify-content-center" >
                                    <button type="submit" class="btn btn-info">Edit Category</button>

                                </div>
                                <!-- /.card-footer -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@stop

