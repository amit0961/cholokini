<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>CholoKini | Online Shopping in Bangladesh for Authentic Products
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('asset/frontend/logofav.png ')}}')}} ">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('asset/frontend/logofav.png ')}}')}} ">
    <link rel=" icon " type="image/png " sizes="16x16 " href="{{asset('asset/frontend/logofav.png ')}}')}} ">
    <link rel=" manifest " href="site.webmanifest ">
    <link rel="mask-icon " color="#fe6a6a " href="safari-pinned-tab.svg ">
    <meta name="msapplication-TileColor " content="#ffffff ">
    <meta name="theme-color " content="#ffffff ">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link href="{{asset('asset/frontend/css/easyzoom.css')}}" rel="stylesheet">
    <link href="{{asset('asset/frontend/css/passtrength.css')}}" rel="stylesheet">
    <link rel="stylesheet " media="screen " href="{{asset('asset/frontend/css/vendor.min.css')}} ">
    <link rel="stylesheet " media="screen " id="main-styles " href="{{asset('asset/frontend/css/theme.min.css')}} ">
</head>
<!-- Body-->
<body>
@include('layouts.frontend.frontendHeader')

@yield('content')
@include('layouts.frontend.frontendFooter')
<!-- Page Content-->
<!-- Hero slider-->

<!-- Footer-->

<!-- JavaScript libraries, plugins and custom scripts -->
<script src="{{asset('asset/frontend/js/easyzoom.js')}}"></script>
<script src="{{asset('asset/frontend/js/passtrength.js')}}"></script>
<script src="{{asset('asset/frontend/js/vendor.min.js')}} "></script>
<script src="{{asset('asset/frontend/js/theme.min.js')}} "></script>
</body>

</html>
