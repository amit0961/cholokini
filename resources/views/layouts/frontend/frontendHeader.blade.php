<?php
use App\Http\Controllers\Controller;
$mainCategories=Controller::mainCategories();
?>
<!-- Quick View Modal-->
<div class="modal-quick-view modal fade" id="quick-view" tabindex="-1">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title product-title"><a href="shop-single-v1.html" data-toggle="tooltip"
                                                         data-placement="right" title="Go to product page">Sports Hooded
                        Sweatshirt<i class="czi-arrow-right font-size-lg ml-2"></i></a></h4>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <!-- Product gallery-->
                    <div class="col-lg-7 pr-lg-0">
                        <div class="cz-product-gallery">
                            <div class="cz-preview order-sm-2">
                                <div class="cz-preview-item active" id="first"><img class="cz-image-zoom"
                                                                                    src="{{asset('asset/frontend/img/shop/single/gallery/01.jpg')}} "
                                                                                    data-zoom="{{asset('asset/frontend/img/shop/single/gallery/01.jpg')}} "
                                                                                    alt="Product image">
                                    <div class="cz-image-zoom-pane"></div>
                                </div>
                                <div class="cz-preview-item" id="second"><img class="cz-image-zoom"
                                                                              src="{{asset('asset/frontend/img/shop/single/gallery/02.jpg')}} "
                                                                              data-zoom="{{asset('asset/frontend/img/shop/single/gallery/02.jpg')}} "
                                                                              alt="Product image">
                                    <div class="cz-image-zoom-pane"></div>
                                </div>
                                <div class="cz-preview-item" id="third"><img class="cz-image-zoom"
                                                                             src="{{asset('asset/frontend/img/shop/single/gallery/03.jpg')}} "
                                                                             data-zoom="{{asset('asset/frontend/img/shop/single/gallery/03.jpg')}} "
                                                                             alt="Product image">
                                    <div class="cz-image-zoom-pane"></div>
                                </div>
                                <div class="cz-preview-item" id="fourth"><img class="cz-image-zoom"
                                                                              src="{{asset('asset/frontend/img/shop/single/gallery/04.jpg')}} "
                                                                              data-zoom="{{asset('asset/frontend/img/shop/single/gallery/04.jpg')}} "
                                                                              alt="Product image">
                                    <div class="cz-image-zoom-pane"></div>
                                </div>
                            </div>
                            <div class="cz-thumblist order-sm-1">
                                <a class="cz-thumblist-item active" href="#first"><img
                                        src="{{asset('asset/frontend/img/shop/single/gallery/th01.jpg')}} " alt="Product thumb"></a>
                                <a class="cz-thumblist-item" href="#second"><img src="{{asset('asset/frontend/img/shop/single/gallery/th02.jpg')}} "
                                                                                 alt="Product thumb"></a>
                                <a class="cz-thumblist-item" href="#third"><img src="{{asset('asset/frontend/img/shop/single/gallery/th03.jpg')}} "
                                                                                alt="Product thumb"></a>
                                <a class="cz-thumblist-item" href="#fourth"><img src="{{asset('asset/frontend/img/shop/single/gallery/th04.jpg')}} "
                                                                                 alt="Product thumb"></a>
                            </div>
                        </div>
                    </div>
                    <!-- Product details-->
                    <div class="col-lg-5 pt-4 pt-lg-0 cz-image-zoom-pane">
                        <div class="product-details ml-auto pb-3">
                            <div class="d-flex justify-content-between align-items-center mb-2">
                                <a href="shop-single-v1.html#reviews">
                                    <div class="star-rating"><i class="sr-star czi-star-filled active"></i><i
                                            class="sr-star czi-star-filled active"></i><i
                                            class="sr-star czi-star-filled active"></i><i
                                            class="sr-star czi-star-filled active"></i><i class="sr-star czi-star"></i>
                                    </div>
                                    <span class="d-inline-block font-size-sm text-body align-middle mt-1 ml-1">74 Reviews</span></a>
                                <button class="btn-wishlist" type="button" data-toggle="tooltip"
                                        title="Add to wishlist"><i class="czi-heart"></i></button>
                            </div>
                            <div class="mb-3"><span class="h3 font-weight-normal text-accent mr-1">$18.<small>99</small></span>
                                <del class="text-muted font-size-lg mr-3">$25.
                                    <small>00</small>
                                </del>
                                <span class="badge badge-danger badge-shadow align-middle mt-n2">Sale</span>
                            </div>
                            <div class="font-size-sm mb-4"><span
                                    class="text-heading font-weight-medium mr-1">Color:</span><span class="text-muted">Red/Dark blue/White</span>
                            </div>
                            <div class="position-relative mr-n4 mb-3">
                                <div class="custom-control custom-option custom-control-inline mb-2">
                                    <input class="custom-control-input" type="radio" name="color" id="color1" checked>
                                    <label class="custom-option-label rounded-circle" for="color1"><span
                                            class="custom-option-color rounded-circle"
                                            style="background-image: url('{{asset('asset/frontend/img/shop/single/color-opt-1.png')}}')"></span></label>
                                </div>
                                <div class="custom-control custom-option custom-control-inline mb-2">
                                    <input class="custom-control-input" type="radio" name="color" id="color2">
                                    <label class="custom-option-label rounded-circle" for="color2"><span
                                            class="custom-option-color rounded-circle"
                                            style="background-image: url('{{asset('asset/frontend/img/shop/single/color-opt-2.png ')}}')"></span></label>
                                </div>
                                <div class="custom-control custom-option custom-control-inline mb-2">
                                    <input class="custom-control-input" type="radio" name="color" id="color3">
                                    <label class="custom-option-label rounded-circle" for="color3"><span
                                            class="custom-option-color rounded-circle"
                                            style="background-image: url('{{asset('asset/frontend/img/shop/single/color-opt-3.png')}}')"></span></label>
                                </div>
                                <div class="product-badge product-available mt-n1"><i class="czi-security-check"></i>Product
                                    available
                                </div>
                            </div>
                            <form class="mb-grid-gutter">
                                <div class="form-group">
                                    <label class="font-weight-medium pb-1" for="product-size">Size:</label>
                                    <select class="custom-select" required id="product-size">
                                        <option value="">Select size</option>
                                        <option value="xs">XS</option>
                                        <option value="s">S</option>
                                        <option value="m">M</option>
                                        <option value="l">L</option>
                                        <option value="xl">XL</option>
                                    </select>
                                </div>
                                <div class="form-group d-flex align-items-center">
                                    <select class="custom-select mr-3" style="width: 5rem;">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </select>
                                    <button class="btn btn-primary btn-shadow btn-block" type="submit"><i
                                            class="czi-cart font-size-lg mr-2"></i>Add to Cart
                                    </button>
                                </div>
                            </form>
                            <h5 class="h6 mb-3 pb-2 border-bottom"><i
                                    class="czi-announcement text-muted font-size-lg align-middle mt-n1 mr-2"></i>Product
                                info</h5>
                            <h6 class="font-size-sm mb-2">Style</h6>
                            <ul class="font-size-sm pl-4">
                                <li>Hooded top</li>
                            </ul>
                            <h6 class="font-size-sm mb-2">Composition</h6>
                            <ul class="font-size-sm pl-4">
                                <li>Elastic rib: Cotton 95%, Elastane 5%</li>
                                <li>Lining: Cotton 100%</li>
                                <li>Cotton 80%, Polyester 20%</li>
                            </ul>
                            <h6 class="font-size-sm mb-2">Art. No.</h6>
                            <ul class="font-size-sm pl-4 mb-0">
                                <li>183260098</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Navbar -->
<header class="box-shadow-sm ">
    <!-- Topbar-->
    <div class="topbar topbar-light bg-faded-light ">
        <div class="container ">
            <div class="topbar-text dropdown d-md-none "><a class="topbar-link dropdown-toggle " href="# "
                                                            data-toggle="dropdown "><i
                        class="czi-support text-muted mr-2 "></i>(+880) 1775239873</a>
                <ul class="dropdown-menu ">
                    <li><a class="dropdown-item " href="tel:+8801775239873 "><i
                                class="czi-support text-muted mr-2 "></i>(+880) 1775239873</a></li>
                </ul>
            </div>
            <div class="topbar-text text-nowrap d-none d-md-inline-block "><i class="czi-support "></i><span
                    class="text-muted mr-1 ">Support</span><a class="topbar-link " href="tel:+8801775239873 ">(+880)
                    1775239873</a></div>
            <div class="cz-carousel cz-controls-static d-none d-md-block ">
                <div class="cz-carousel-inner "
                     data-carousel-options="{&quot;mode&quot;: &quot;gallery&quot;, &quot;nav&quot;: false} ">
                    <div class="topbar-text ">Inside Dhaka Free Fast Shipping.</div>
                    <div class="topbar-text ">Promises Fastest Deliver in 72 hrs.</div>
                    <div class="topbar-text ">Friendly (9am-11pm) customer support.</div>
                </div>
            </div>
        </div>
    </div>
    <!-- NAVBAR STARTS.-->
    <div class="navbar-sticky bg-light ">
        <div class="navbar navbar-expand-lg navbar-light ">
            <div class="container ">
                <a class="navbar-brand d-none d-sm-block mr-3 flex-shrink-0 " href="{{url('/')}}"
                   style="min-width: 7rem; "><img width="142 " src="{{asset('asset/frontend/img/logo.png ')}} " alt="CholoKini "/></a>
                <a class="navbar-brand d-sm-none mr-2 " href="{{url('/')}} " style="min-width: 4.625rem; "><img
                        width="74 " src="{{asset('asset/frontend/img/logo.png ')}}" alt="CholoKini "/></a>
                <!-- Search-->
                <div class="input-group-overlay d-none d-lg-block mx-4">
                    <div class="input-group-prepend-overlay"><span class="input-group-text"><i
                                class="czi-search"></i></span></div>
                    <input class="form-control prepended-form-control appended-form-control" type="text"
                           placeholder="Search for products">
                    <div class="input-group-append-overlay">
                        <select class="custom-select">
                            <option>All categories</option>

{{--                            @foreach($mainCategories as $category)--}}
{{--                            <option>{{$category->name}}</option>--}}
{{--                            @endforeach--}}
                        </select>
                    </div>
                </div>
                <!-- Toolbar-->
                <div class="navbar-toolbar d-flex flex-shrink-0 align-items-center">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse">
                        <span class="navbar-toggler-icon"></span></button>
                    <a class="navbar-tool navbar-stuck-toggler" href="#"><span
                            class="navbar-tool-tooltip">Expand menu</span>
                        <div class="navbar-tool-icon-box"><i class="navbar-tool-icon czi-menu"></i></div>
                    </a>
                    @if(empty(Auth::check()))
                        <a class="navbar-tool ml-1 ml-lg-0 mr-n1 mr-lg-2" href="{{url('/loginRegister')}}" >
                            <div class="navbar-tool-icon-box"><i class="navbar-tool-icon czi-user"></i></div>
                            <div class="navbar-tool-text ml-n3">
                                <small>Hello, Sign in</small>
                                Log In <i class="czi-star"></i>
                            </div>
                        </a>
                    @else
                        <a class="navbar-tool ml-1 ml-lg-0 mr-n1 mr-lg-2" href="{{url('/userAccount')}}" >
                            <div class="navbar-tool-icon-box"><i class="navbar-tool-icon czi-user"></i></div>
                            <div class="navbar-tool-text ml-n3">
                                <small>Logged In...</small>
                                My Account<i class="czi-document"></i>
                            </div>
                        </a>

                    @endif

                    <div class="navbar-tool dropdown ml-3"><a class="navbar-tool-icon-box bg-secondary dropdown-toggle"
                                                              href="shop-cart.html"><span
                                class="navbar-tool-label">4</span><i class="navbar-tool-icon czi-cart"></i></a><a
                            class="navbar-tool-text" href="{{url('/cart')}}">
                            <small>My Cart</small>
                            $1,247.00</a>
                        <!-- Cart dropdown-->
                        <div class="dropdown-menu dropdown-menu-right" style="width: 20rem;">
                            <div class="widget widget-cart px-3 pt-2 pb-3">
                                <div style="height: 15rem;" data-simplebar data-simplebar-auto-hide="false">
                                    <div class="widget-cart-item pb-2 border-bottom">
                                        <button class="close text-danger" type="button" aria-label="Remove"><span
                                                aria-hidden="true">&times;</span></button>
                                        <div class="media align-items-center">
                                            <a class="d-block mr-2" href="shop-single-v2.html"><img width="64"
                                                                                                    src="{{asset('asset/frontend/img/shop/cart/widget/05.jpg')}} "
                                                                                                    alt="Product"/></a>
                                            <div class="media-body">
                                                <h6 class="widget-product-title"><a href="shop-single-v2.html">Bluetooth
                                                        Headphones</a></h6>
                                                <div class="widget-product-meta"><span class="text-accent mr-2">$259.<small>00</small></span><span
                                                        class="text-muted">x 1</span></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="widget-cart-item py-2 border-bottom">
                                        <button class="close text-danger" type="button" aria-label="Remove"><span
                                                aria-hidden="true">&times;</span></button>
                                        <div class="media align-items-center">
                                            <a class="d-block mr-2" href="shop-single-v2.html"><img width="64"
                                                                                                    src="{{asset('asset/frontend/img/shop/cart/widget/06.jpg')}} "
                                                                                                    alt="Product"/></a>
                                            <div class="media-body">
                                                <h6 class="widget-product-title"><a href="shop-single-v2.html">Cloud
                                                        Security Camera</a></h6>
                                                <div class="widget-product-meta"><span class="text-accent mr-2">$122.<small>00</small></span><span
                                                        class="text-muted">x 1</span></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="widget-cart-item py-2 border-bottom">
                                        <button class="close text-danger" type="button" aria-label="Remove"><span
                                                aria-hidden="true">&times;</span></button>
                                        <div class="media align-items-center">
                                            <a class="d-block mr-2" href="shop-single-v2.html"><img width="64"
                                                                                                    src="{{asset('asset/frontend/img/shop/cart/widget/07.jpg')}} "
                                                                                                    alt="Product"/></a>
                                            <div class="media-body">
                                                <h6 class="widget-product-title"><a href="shop-single-v2.html">Android
                                                        Smartphone S10</a></h6>
                                                <div class="widget-product-meta"><span class="text-accent mr-2">$799.<small>00</small></span><span
                                                        class="text-muted">x 1</span></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="widget-cart-item py-2 border-bottom">
                                        <button class="close text-danger" type="button" aria-label="Remove"><span
                                                aria-hidden="true">&times;</span></button>
                                        <div class="media align-items-center">
                                            <a class="d-block mr-2" href="shop-single-v2.html"><img width="64"
                                                                                                    src="{{asset('asset/frontend/img/shop/cart/widget/08.jpg')}} "
                                                                                                    alt="Product"/></a>
                                            <div class="media-body">
                                                <h6 class="widget-product-title"><a href="shop-single-v2.html">Android
                                                        Smart TV Box</a></h6>
                                                <div class="widget-product-meta"><span class="text-accent mr-2">$67.<small>00</small></span><span
                                                        class="text-muted">x 1</span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex flex-wrap justify-content-between align-items-center py-3">
                                    <div class="font-size-sm mr-2 py-2"><span class="text-muted">Subtotal:</span><span
                                            class="text-accent font-size-base ml-1">$1,247.<small>00</small></span>
                                    </div>
                                    <a class="btn btn-outline-secondary btn-sm" href="shop-cart.html">Expand cart<i
                                            class="czi-arrow-right ml-1 mr-n1"></i></a>
                                </div>
                                <a class="btn btn-primary btn-sm btn-block" href="checkout-details.html"><i
                                        class="czi-card mr-2 font-size-base align-middle"></i>Checkout</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="navbar navbar-expand-lg navbar-light navbar-stuck-menu mt-n2 pt-0 pb-2">
            <div class="container">
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <!-- Search-->
                    <div class="input-group-overlay d-lg-none my-3">
                        <div class="input-group-prepend-overlay"><span class="input-group-text"><i
                                    class="czi-search"></i></span></div>
                        <input class="form-control prepended-form-control" type="text"
                               placeholder="Search for products">
                    </div>
                    <!-- Departments menu-->
                    <ul class="navbar-nav mega-nav pr-lg-2 mr-lg-2">
                        <li class="nav-item dropdown"><a class="nav-link dropdown-toggle pl-0" href="#"
                                                         data-toggle="dropdown"><i
                                    class="czi-menu align-middle mt-n1 mr-2"></i>All Categories</a>

                            <ul class="dropdown-menu">
                                @foreach($mainCategories as $category)
                                <li class="dropdown mega-dropdown"><a class="dropdown-item dropdown-toggle" href="{{asset('/products/'.$category->url)}}"
                                                                      data-toggle="dropdown"><i
                                            class="czi-star-filled opacity-60 font-size-lg mt-n1 mr-2"></i>{{$category->name}}</a>
                                    <div class="dropdown-menu p-0">
                                        <div class="d-flex flex-wrap flex-md-nowrap px-2">
                                            <div class="mega-dropdown-column py-4 px-3">
                                                <div class="widget widget-links">
                                                    <h6 class="font-size-base mb-3">Computers</h6>
                                                    <ul class="widget-list">
                                                        <li class="widget-list-item pb-1"><a class="widget-list-link"
                                                                                             href="#">Laptops &amp;
                                                                Tablets</a></li>
                                                        <li class="widget-list-item pb-1"><a class="widget-list-link"
                                                                                             href="#">Desktop
                                                                Computers</a></li>
                                                        <li class="widget-list-item pb-1"><a class="widget-list-link"
                                                                                             href="#">Computer External
                                                                Components</a></li>
                                                        <li class="widget-list-item pb-1"><a class="widget-list-link"
                                                                                             href="#">Computer Internal
                                                                Components</a></li>
                                                        <li class="widget-list-item pb-1"><a class="widget-list-link"
                                                                                             href="#">Networking
                                                                Products (NAS)</a></li>
                                                        <li class="widget-list-item pb-1"><a class="widget-list-link"
                                                                                             href="#">Single Board
                                                                Computers</a></li>
                                                        <li class="widget-list-item pb-1"><a class="widget-list-link"
                                                                                             href="#">Desktop
                                                                Barebones</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="mega-dropdown-column py-4 px-3">
                                                <div class="widget widget-links">
                                                    <h6 class="font-size-base mb-3">Accessories</h6>
                                                    <ul class="widget-list">
                                                        <li class="widget-list-item pb-1"><a class="widget-list-link"
                                                                                             href="#">Monitors</a></li>
                                                        <li class="widget-list-item pb-1"><a class="widget-list-link"
                                                                                             href="#">Bags, Cases &amp;
                                                                Sleeves</a></li>
                                                        <li class="widget-list-item pb-1"><a class="widget-list-link"
                                                                                             href="#">Batteries</a></li>
                                                        <li class="widget-list-item pb-1"><a class="widget-list-link"
                                                                                             href="#">Charges &amp;
                                                                Adapters</a></li>
                                                        <li class="widget-list-item pb-1"><a class="widget-list-link"
                                                                                             href="#">Cooling Pads</a>
                                                        </li>
                                                        <li class="widget-list-item pb-1"><a class="widget-list-link"
                                                                                             href="#">Mounts</a></li>
                                                        <li class="widget-list-item pb-1"><a class="widget-list-link"
                                                                                             href="#">Replacement
                                                                Screens</a></li>
                                                        <li class="widget-list-item pb-1"><a class="widget-list-link"
                                                                                             href="#">Security Locks</a>
                                                        </li>
                                                        <li class="widget-list-item pb-1"><a class="widget-list-link"
                                                                                             href="#">Stands</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="mega-dropdown-column d-none d-lg-block py-4 text-center">
                                                <a class="d-block mb-2" href="#"><img src="{{asset('asset/frontend/img/shop/departments/07.jpg')}} "
                                                                                      alt="Computers & Accessories"/></a>
                                                <div class="font-size-sm mb-3">Starting from <span
                                                        class='font-weight-medium'>$149.<small>80</small></span></div>
                                                <a class="btn btn-primary btn-shadow btn-sm" href="#">See offers<i
                                                        class="czi-arrow-right font-size-xs ml-1"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </li>

                            </ul>

                        </li>
                    </ul>
                    <!-- Primary menu-->
                    <ul class="navbar-nav">
                        <li class="nav-item ">
                            <a class="nav-link " href="{{url('/')}}">Home <i class="czi-home"></i></a>
                        </li>
                        <li class="nav-item dropdown active">
                            <a class="nav-link dropdown-toggle" href="#"
                                                                data-toggle="dropdown">SUPER DEAL <i class="czi-star-filled"></i> </a>
                            <div class="dropdown-menu p-0">
                                <div class="d-flex flex-wrap flex-md-nowrap px-2 ">
                                    <div class="mega-dropdown-column py-4 px-3">
                                        <div class="widget widget-links mb-3">
                                            <a class="d-block overflow-hidden rounded-lg mb-3 " href="{{url('/')}} "><img
                                                    src="{{asset('asset/frontend/img/shop/departments/01.jpg')}}  " alt="Shoes "/></a>
                                            <h6 class="font-size-base mb-2 ">Clothing</h6>
                                            <ul class="widget-list ">
                                                <li class="widget-list-item "><a class="widget-list-link " href="# ">Women's
                                                        clothing</a></li>
                                                <li class="widget-list-item "><a class="widget-list-link " href="# ">Men's
                                                        clothing</a></li>
                                                <li class="widget-list-item "><a class="widget-list-link " href="# ">Kid's
                                                        clothing</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="mega-dropdown-column py-4 px-3">
                                        <div class="widget widget-links">
                                            <a class="d-block overflow-hidden rounded-lg mb-3 " href="# "><img
                                                    src="{{asset('asset/frontend/img/shop/departments/02.jpg')}}  " alt="Shoes "/></a>
                                            <h6 class="font-size-base mb-2 ">Shoes</h6>
                                            <ul class="widget-list ">
                                                <li class="widget-list-item "><a class="widget-list-link " href="# ">Women's
                                                        shoes</a></li>
                                                <li class="widget-list-item "><a class="widget-list-link " href="# ">Men's
                                                        shoes</a></li>
                                                <li class="widget-list-item "><a class="widget-list-link " href="# ">Kid's
                                                        shoes</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="mega-dropdown-column py-4 pr-3">
                                        <div class="widget widget-links ">
                                            <a class="d-block overflow-hidden rounded-lg mb-3 " href="# "><img
                                                    src="{{asset('asset/frontend/img/shop/departments/03.jpg')}}  " alt="Shoes "/></a>
                                            <h6 class="font-size-base mb-2 ">Gadgets</h6>
                                            <ul class="widget-list ">
                                                <li class="widget-list-item "><a class="widget-list-link " href="# ">Smartphones
                                                        &amp; Tablets</a></li>
                                                <li class="widget-list-item "><a class="widget-list-link " href="# ">Wearable
                                                        gadgets</a></li>
                                                <li class="widget-list-item "><a class="widget-list-link " href="# ">E-book
                                                        readers</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex flex-wrap flex-md-nowrap px-2 ">
                                    <div class="mega-dropdown-column py-4 px-3">
                                        <div class="widget widget-links mb-3">
                                            <a class="d-block overflow-hidden rounded-lg mb-3 " href="# "><img
                                                    src="{{asset('asset/frontend/img/shop/departments/04.jpg')}}  " alt="Shoes "/></a>
                                            <h6 class="font-size-base mb-2 ">Furniture &amp; Decor</h6>
                                            <ul class="widget-list ">
                                                <li class="widget-list-item "><a class="widget-list-link " href="# ">Home
                                                        furniture</a></li>
                                                <li class="widget-list-item "><a class="widget-list-link " href="# ">Office
                                                        furniture</a></li>
                                                <li class="widget-list-item "><a class="widget-list-link " href="# ">Lighting
                                                        and decoration</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="mega-dropdown-column py-4 px-3">
                                        <div class="widget widget-links">
                                            <a class="d-block overflow-hidden rounded-lg mb-3 " href="# "><img
                                                    src="{{asset('asset/frontend/img/shop/departments/05.jpg')}}  " alt="Shoes "/></a>
                                            <h6 class="font-size-base mb-2 ">Accessories</h6>
                                            <ul class="widget-list ">
                                                <li class="widget-list-item "><a class="widget-list-link " href="# ">Hats</a>
                                                </li>
                                                <li class="widget-list-item "><a class="widget-list-link " href="# ">Sunglasses</a>
                                                </li>
                                                <li class="widget-list-item "><a class="widget-list-link " href="# ">Bags</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="mega-dropdown-column py-4 pr-3">
                                        <div class="widget widget-links ">
                                            <a class="d-block overflow-hidden rounded-lg mb-3 " href="# "><img
                                                    src="{{asset('asset/frontend/img/shop/departments/06.jpg')}}  " alt="Shoes "/></a>
                                            <h6 class="font-size-base mb-2 ">Entertainment</h6>
                                            <ul class="widget-list ">
                                                <li class="widget-list-item "><a class="widget-list-link " href="# ">Kid's
                                                        toys</a></li>
                                                <li class="widget-list-item "><a class="widget-list-link " href="# ">Video
                                                        games</a></li>
                                                <li class="widget-list-item "><a class="widget-list-link " href="# ">Outdoor
                                                        / Camping</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="#"
                                                         data-toggle="dropdown">Need Help <i class="czi-mobile"></i></a>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item " href="{{url('/about/')}}" >About Us</a></li>
                                <li><a class="dropdown-item " href="{{url('/contact/')}}" >Contacts</a></li>
                                <li class="dropdown"><a class="dropdown-item dropdown-toggle" href="#"
                                                        data-toggle="dropdown">Help Center</a>
                                    <ul class="dropdown-menu">
                                        <li><a class="dropdown-item" href="#">Phone:- +8801775239873</a></li>
                                        <li><a class="dropdown-item" href="#">E-Mail:- samplebd@gmail.com</a></li>
                                        <li><a class="dropdown-item" href="#">Complain about Product:- complain@cholokini.com </a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown "><a class="nav-link dropdown-toggle" href="#"
                                                          data-toggle="dropdown">Policy & Conditions <i class="czi-security-prohibition"></i></a>
                            <ul class="dropdown-menu">
                                <li class="dropdown"><a class="dropdown-item " href="#" data-toggle="dropdown">Return &
                                        Replace Policy</a></li>

                                <li class="dropdown"><a class="dropdown-item " href="#" data-toggle="dropdown">Method
                                        Policy</a>

                                </li>
                                <li class="dropdown"><a class="dropdown-item " href="#" data-toggle="dropdown">Privacy
                                        Policy & FAQ</a>
                                </li>
                            </ul>
                        </li>
                            @if(empty(Auth::check()))
                            <li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="{{url('/loginRegister')}}">LogIn <i class="czi-sign-in"></i></a></li>
                            @else
                            <li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="{{url('/userAccount')}}">My Account <i class="czi-user"></i> </a></li>
                            <li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="{{url('/logout')}}">Logout <i class="czi-sign-out"></i></a></li>
                            @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>
