@extends('layouts.frontend.frontendDesign')
@section('content')
    <!-- Page Title (Shop)-->
    <div class="page-title-overlap bg-dark pt-4">
        <div class="container d-lg-flex justify-content-between py-2 py-lg-3">
            <div class="order-lg-2 mb-3 mb-lg-0 pt-lg-2">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-light flex-lg-nowrap justify-content-center justify-content-lg-star">
                        <li class="breadcrumb-item"><a class="text-nowrap" href="index.html"><i class="czi-home"></i>Home</a></li>
                        <li class="breadcrumb-item text-nowrap"><a href="shop-grid-ls.html">Shop</a>
                        </li>
                        <li class="breadcrumb-item text-nowrap active" aria-current="page">Cart</li>
                    </ol>
                </nav>
            </div>
            <div class="order-lg-1 pr-lg-4 text-center text-lg-left">
                <h1 class="h3 text-light mb-0 border-bottom">My Shopping Cart</h1>
            </div>
        </div>
    </div>
    <!-- Page Content-->
    <div class="container pb-5 mb-2 mb-md-4">
        <div class="row">
            <!-- List of items-->
            <section class="col-lg-8">
                <div class="d-flex justify-content-between align-items-center pt-3 pb-2 pb-sm-5 mt-1">
                    <h2 class="h6 text-light mb-0">Products</h2><a class="btn btn-outline-primary btn-sm pl-2" href="{{'/'}}"><i class="czi-arrow-left mr-2"></i>Continue shopping</a>
                </div>
                <!-- Item-->
                @if (Session::get('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{!! session('success') !!}</strong>
                    </div>
                @endif
                @if (Session::get('error'))
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{!! session('error') !!}</strong>
                    </div>
                @endif
                <?php $totalAmount =0; ?>

                @foreach($userCart as $cart)

                    <div class="d-sm-flex justify-content-between align-items-center my-4 pb-3 border-bottom">
                        <div class="media media-ie-fix d-block d-sm-flex align-items-center text-center text-sm-left">
                            <a class="d-inline-block mx-auto mr-sm-4" href="" style="width: 10rem;"><img src="{{asset('asset/backend/images/products/small/'.$cart->image)}}" alt="Product"></a>
                                <div class="media-body pt-2">
                                <h3 class="product-title font-size-base mb-2"><a href="shop-single-v1.html">{{$cart->product_name}}</a></h3>
                                <div class="font-size-sm"><span class="text-muted mr-2">Size:</span>{{$cart->size}}</div>
                                <div class="font-size-sm"><span class="text-muted mr-2">Color:</span>{{$cart->product_color}}</div>
                                <div class="font-size-lg text-accent pt-2">৳ {{$cart->price*$cart->quantity}}.<small>00</small></div>
                            </div>
                        </div>
                        <div class="pt-2 pt-sm-0 pl-sm-3 mx-auto mx-sm-0 text-center text-sm-left" style="max-width: 15rem;">
                            <div class="input-group btn-block">
                                <span class="input-group-btn">
                                  <a href="{{url('/cart/updateCartQuantity/'.$cart->id.'/1')}}" ><button class="btn btn-success" title="" data-toggle="tooltip" type="submit" data-original-title="Add"><i class="fa fa-plus"></i></button></a>
                                </span>
                                <input type="text" class="form-control quantity" size="1" value="{{$cart->quantity}}" name="quantity" id="quantity1">
                                <span class="input-group-btn">
                                    @if($cart->quantity>1)
                                        <a href="{{url('/cart/updateCartQuantity/'.$cart->id.'/-1')}} " ><button class="btn btn-warning" title="" data-toggle="tooltip" type="submit" data-original-title="Subtract"><i class="fa fa-minus"></i></button> </a>
                                    @endif
                                  <a href="{{url('/cart/deleteCartProduct/'.$cart->id)}}" ><button  class="btn btn-danger" title="" data-toggle="tooltip" type="button" data-original-title="Remove"><i class="fa fa-times-circle"></i></button></a>
                                </span>
                            </div>
                        </div>
                    </div>
                    <?php $totalAmount=$totalAmount+($cart->price*$cart->quantity); ?>
                @endforeach

            </section>
            <!-- Sidebar-->
            <aside class="col-lg-4 pt-4 pt-lg-0">
                <div class="cz-sidebar-static rounded-lg box-shadow-lg ml-lg-auto">
                    <div class="accordion" id="order-options">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="accordion-heading"><a href="#promo-code" role="button" data-toggle="collapse" aria-expanded="true" aria-controls="promo-code">Apply Coupon Code<span class="accordion-indicator"></span></a></h3>
                            </div>
                            <div class="collapse show" id="promo-code" data-parent="#order-options">
                                <form action="{{url('/cart/applyCoupon')}}" class="card-body needs-validation" method="post" novalidate>
                                    {{csrf_field()}}
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="coupon_code" id="coupon_code" placeholder="Coupon code" required>
                                        <div class="invalid-feedback">Please provide coupon code.</div>
                                    </div>
                                    <button class="btn btn-outline-primary btn-block" type="submit">Apply Coupon Code</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="text-center mt-5 mb-4 pb-3 border-bottom">
                        @if(!empty(Session::get('CouponAmount')))
                            <h2 class="h6 mb-2 pb-1">You Have To Pay</h2>
                            <h3 class="font-weight-normal mb-3">= ৳ <?php echo $totalAmount- Session::get('CouponAmount'); ?>.<small>00</small></h3>

                            <ul class="list-unstyled font-size-sm pb-2 border-bottom mt-5">
                                <li class="d-flex justify-content-between align-items-center"><span class="mr-2">Subtotal:</span><span class="text-right">৳ <?php echo $totalAmount; ?>.<small>00</small></span></li>
                                <li class="d-flex justify-content-between align-items-center"><span class="mr-2 text-bold">Discount:</span><span class="text-right">- ৳ <?php echo Session::get('CouponAmount'); ?></span></li>
                                <li class="d-flex justify-content-between align-items-center"><span class="mr-2">Shipping:</span><span class="text-right">—</span></li>
                            </ul>
                        @else
                            <h2 class="h6 mb-2 pb-1">You Have To Pay</h2>
                            <h3 class="font-weight-normal mb-3">= ৳ <?php echo $totalAmount ?>.<small>00</small></h3>

                            <ul class="list-unstyled font-size-sm pb-2 border-bottom mt-5">
                                <li class="d-flex justify-content-between align-items-center"><span class="mr-2">Subtotal:</span><span class="text-right">৳ <?php echo $totalAmount; ?>.<small>00</small></span></li>
                                <li class="d-flex justify-content-between align-items-center"><span class="mr-2">Shipping:</span><span class="text-right">—</span></li>
                            </ul>
                        @endif
                    </div>


                    <a class="btn btn-primary btn-shadow btn-block mt-4" href="checkout-details.html"><i class="czi-card font-size-lg mr-2"></i>Proceed to Checkout</a>
                </div>
            </aside>
        </div>
    </div>
@stop
