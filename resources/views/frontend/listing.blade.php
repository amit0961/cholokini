@extends('layouts.frontend.frontendDesign')
@section('content')
    <section class="cz-carousel cz-controls-lg ">
        <div class="cz-carousel-inner ">

            <!-- Item-->
            <div class="px-lg-5 " style="background-color: #3aafd2; ">
                <div class="d-lg-flex justify-content-between align-items-center pl-lg-4 "><img
                        class="d-block order-lg-2 mr-lg-n5 flex-shrink-0 " src="{{asset('asset/frontend/img/home/hero-slider/01.jpg')}}  "
                        alt="Summer Collection ">
                    <div class="position-relative mx-auto mr-lg-n5 py-5 px-4 mb-lg-5 order-lg-1 "
                         style="max-width: 42rem; z-index: 10; ">
                        <div class="pb-lg-5 mb-lg-5 text-center text-lg-left text-lg-nowrap ">
                            <h2 class="text-light font-weight-light pb-1 from-left ">Has just arrived!</h2>
                            <h1 class="text-light display-4 from-left delay-1 ">Huge Summer Collection</h1>
                            <p class="font-size-lg text-light pb-3 from-left delay-2 ">Swimwear, Tops, Shorts, Sunglasses
                                &amp; much more...</p><a class="btn btn-primary scale-up delay-4 "
                                                         href="shop-grid-ls.html ">Shop Now<i class="czi-arrow-right
        ml-2 mr-n1 "></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Item-->
            <div class="px-lg-5 " style="background-color: #f5b1b0; ">
                <div class="d-lg-flex justify-content-between align-items-center pl-lg-4 "><img
                        class="d-block order-lg-2 mr-lg-n5 flex-shrink-0 " src="{{asset('asset/frontend/img/home/hero-slider/02.jpg')}}  "
                        alt="Women Sportswear ">
                    <div class="position-relative mx-auto mr-lg-n5 py-5 px-4 mb-lg-5 order-lg-1 "
                         style="max-width: 42rem; z-index: 10; ">
                        <div class="pb-lg-5 mb-lg-5 text-center text-lg-left text-lg-nowrap ">
                            <h2 class="text-light font-weight-light pb-1 from-bottom ">Hurry up! Limited time offer.</h2>
                            <h1 class="text-light display-4 from-bottom delay-1 ">Women Sportswear Sale</h1>
                            <p class="font-size-lg text-light pb-3 from-bottom delay-2 ">Sneakers, Keds, Sweatshirts,
                                Hoodies &amp; much more...</p><a class="btn btn-primary scale-up delay-4 "
                                                                 href="shop-grid-ls.html ">Shop Now<i class="czi-arrow-right
        ml-2 mr-n1 "></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Item-->
            <div class="px-lg-5 " style="background-color: #eba170; ">
                <div class="d-lg-flex justify-content-between align-items-center pl-lg-4 "><img
                        class="d-block order-lg-2 mr-lg-n5 flex-shrink-0 " src="{{asset('asset/frontend/img/home/hero-slider/03.jpg')}}  "
                        alt="Men Accessories ">
                    <div class="position-relative mx-auto mr-lg-n5 py-5 px-4 mb-lg-5 order-lg-1 "
                         style="max-width: 42rem; z-index: 10; ">
                        <div class="pb-lg-5 mb-lg-5 text-center text-lg-left text-lg-nowrap ">
                            <h2 class="text-light font-weight-light pb-1 from-top ">Complete your look with</h2>
                            <h1 class="text-light display-4 from-top delay-1 ">New Men's Accessories</h1>
                            <p class="font-size-lg text-light pb-3 from-top delay-2 ">Hats &amp; Caps, Sunglasses, Bags
                                &amp; much more...</p><a class="btn btn-primary scale-up delay-4 "
                                                         href="shop-grid-ls.html ">Shop Now<i class="czi-arrow-right ml-2
        mr-n1 "></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Popular categories-->
    <section class=" container position-relative mb-2 pt-3 pt-lg-0 pb-5 mt-lg-n10 " style="z-index: 10; ">
        <div class="row ">
            <div class="col-xl-8 col-lg-9 ">
                <div class="card border-0 bg-faded-dark box-shadow-lg ">
                    <div class="card-body px-3 pt-grid-gutter pb-0 ">
                        <div class="row no-gutters pl-1 ">
                            <div class="col-sm-4 px-2 mb-grid-gutter ">
                                <a class="d-block text-center text-decoration-none mr-1 " href="# "><img
                                        class="d-block rounded mb-3 " src="{{asset('asset/frontend/img/home/categories/cat-sm01.jpg')}}  " alt="Men ">
                                    <h3 class="font-size-base pt-1 mb-0 ">Men</h3>
                                </a>
                            </div>
                            <div class="col-sm-4 px-2 mb-grid-gutter ">
                                <a class="d-block text-center text-decoration-none mr-1 " href="#"><img
                                        class="d-block rounded mb-3 " src="{{asset('asset/frontend/img/home/categories/cat-sm02.jpg')}}  " alt="Women ">
                                    <h3 class="font-size-base pt-1 mb-0 ">Women</h3>
                                </a>
                            </div>
                            <div class="col-sm-4 px-2 mb-grid-gutter ">
                                <a class="d-block text-center text-decoration-none mr-1 " href="#"><img
                                        class="d-block rounded mb-3 " src="{{asset('asset/frontend/img/home/categories/cat-sm03.jpg')}}  " alt="Kids ">
                                    <h3 class="font-size-base pt-1 mb-0 ">Kids</h3>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Products grid (Trending products)-->
    <div class="container pb-5 mb-2 mb-md-4 ">
        <div class="row ">
            <aside class="col-lg-4">
                <!-- Sidebar-->
                <div class="cz-sidebar rounded-lg box-shadow-lg" id="shop-sidebar">
                    <div class="cz-sidebar-header box-shadow-sm">
                        <button class="close ml-auto" type="button" data-dismiss="sidebar" aria-label="Close"><span
                                class="d-inline-block font-size-xs font-weight-normal align-middle">Close sidebar</span><span
                                class="d-inline-block align-middle ml-2" aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="cz-sidebar-body" data-simplebar data-simplebar-auto-hide="true">
                        <!-- Categories-->
                        <div class="widget widget-categories mb-4 pb-4 border-bottom">
                            <h3 class="widget-title">Categories</h3>
                            <div class="accordion mt-n1" id="shop-categories">
                                @foreach($categories as $category)
                                    <div class="card">
                                        <div class="card-header">
                                            <h3 class="accordion-heading"><a class="collapsed" href="#{{$category->name}}" role="button"
                                                                             data-toggle="collapse" aria-expanded="false"
                                                                             aria-controls="shoes">{{$category->name}}<span
                                                        class="accordion-indicator"></span></a></h3>
                                        </div>
                                        <div class="collapse" id="{{$category->name}}" data-parent="#shop-categories">
                                            <div class="card-body">
                                                <div class="widget widget-links cz-filter">
                                                    <div class="input-group-overlay input-group-sm mb-2">
                                                        <input
                                                            class="cz-filter-search form-control form-control-sm appended-form-control"
                                                            type="text" placeholder="Search">
                                                        <div class="input-group-append-overlay"><span
                                                                class="input-group-text"><i class="czi-search"></i></span></div>
                                                    </div>
                                                    <ul class="widget-list cz-filter-list pt-1" style="height: 12rem;"
                                                        data-simplebar data-simplebar-auto-hide="false">
                                                        <li class="widget-list-item cz-filter-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span class="cz-filter-item-text">View all</span><span
                                                                    class="font-size-xs text-muted ml-3">1,953</span></a></li>
                                                        @foreach($category->categories as $subcategory)
                                                            <li class="widget-list-item cz-filter-item"><a
                                                                    class="widget-list-link d-flex justify-content-between align-items-center"
                                                                    href="{{asset('/products/'.$subcategory->url)}}"><span class="cz-filter-item-text">{{$subcategory->name}}</span><span
                                                                        class="font-size-xs text-muted ml-3">247</span></a></li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <!-- Filter by Brand-->
                        <div class="widget cz-filter mb-4 pb-4 border-bottom">
                            <h3 class="widget-title">Brand</h3>
                            <div class="input-group-overlay input-group-sm mb-2">
                                <input class="cz-filter-search form-control form-control-sm appended-form-control"
                                       type="text" placeholder="Search">
                                <div class="input-group-append-overlay"><span class="input-group-text"><i
                                            class="czi-search"></i></span></div>
                            </div>
                            <ul class="widget-list cz-filter-list list-unstyled pt-1" style="max-height: 12rem;"
                                data-simplebar data-simplebar-auto-hide="false">
                                <li class="cz-filter-item d-flex justify-content-between align-items-center mb-1">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="adidas">
                                        <label class="custom-control-label cz-filter-item-text" for="adidas">Adidas</label>
                                    </div>
                                    <span class="font-size-xs text-muted">425</span>
                                </li>
                                <li class="cz-filter-item d-flex justify-content-between align-items-center mb-1">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="ataylor">
                                        <label class="custom-control-label cz-filter-item-text" for="ataylor">Ann
                                            Taylor</label>
                                    </div>
                                    <span class="font-size-xs text-muted">15</span>
                                </li>
                                <li class="cz-filter-item d-flex justify-content-between align-items-center mb-1">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="armani">
                                        <label class="custom-control-label cz-filter-item-text" for="armani">Armani</label>
                                    </div>
                                    <span class="font-size-xs text-muted">18</span>
                                </li>
                                <li class="cz-filter-item d-flex justify-content-between align-items-center mb-1">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="banana">
                                        <label class="custom-control-label cz-filter-item-text" for="banana">Banana
                                            Republic</label>
                                    </div>
                                    <span class="font-size-xs text-muted">103</span>
                                </li>
                                <li class="cz-filter-item d-flex justify-content-between align-items-center mb-1">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="bilabong">
                                        <label class="custom-control-label cz-filter-item-text"
                                               for="bilabong">Bilabong</label>
                                    </div>
                                    <span class="font-size-xs text-muted">27</span>
                                </li>
                                <li class="cz-filter-item d-flex justify-content-between align-items-center mb-1">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="birkenstock">
                                        <label class="custom-control-label cz-filter-item-text" for="birkenstock">Birkenstock</label>
                                    </div>
                                    <span class="font-size-xs text-muted">10</span>
                                </li>
                                <li class="cz-filter-item d-flex justify-content-between align-items-center mb-1">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="klein">
                                        <label class="custom-control-label cz-filter-item-text" for="klein">Calvin
                                            Klein</label>
                                    </div>
                                    <span class="font-size-xs text-muted">365</span>
                                </li>
                                <li class="cz-filter-item d-flex justify-content-between align-items-center mb-1">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="columbia">
                                        <label class="custom-control-label cz-filter-item-text"
                                               for="columbia">Columbia</label>
                                    </div>
                                    <span class="font-size-xs text-muted">508</span>
                                </li>
                                <li class="cz-filter-item d-flex justify-content-between align-items-center mb-1">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="converse">
                                        <label class="custom-control-label cz-filter-item-text"
                                               for="converse">Converse</label>
                                    </div>
                                    <span class="font-size-xs text-muted">176</span>
                                </li>
                                <li class="cz-filter-item d-flex justify-content-between align-items-center mb-1">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="dockers">
                                        <label class="custom-control-label cz-filter-item-text"
                                               for="dockers">Dockers</label>
                                    </div>
                                    <span class="font-size-xs text-muted">54</span>
                                </li>
                                <li class="cz-filter-item d-flex justify-content-between align-items-center mb-1">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="fruit">
                                        <label class="custom-control-label cz-filter-item-text" for="fruit">Fruit of the
                                            Loom</label>
                                    </div>
                                    <span class="font-size-xs text-muted">739</span>
                                </li>
                                <li class="cz-filter-item d-flex justify-content-between align-items-center mb-1">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="hanes">
                                        <label class="custom-control-label cz-filter-item-text" for="hanes">Hanes</label>
                                    </div>
                                    <span class="font-size-xs text-muted">92</span>
                                </li>
                                <li class="cz-filter-item d-flex justify-content-between align-items-center mb-1">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="choo">
                                        <label class="custom-control-label cz-filter-item-text" for="choo">Jimmy
                                            Choo</label>
                                    </div>
                                    <span class="font-size-xs text-muted">17</span>
                                </li>
                                <li class="cz-filter-item d-flex justify-content-between align-items-center mb-1">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="levis">
                                        <label class="custom-control-label cz-filter-item-text" for="levis">Levi's</label>
                                    </div>
                                    <span class="font-size-xs text-muted">361</span>
                                </li>
                                <li class="cz-filter-item d-flex justify-content-between align-items-center mb-1">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="lee">
                                        <label class="custom-control-label cz-filter-item-text" for="lee">Lee</label>
                                    </div>
                                    <span class="font-size-xs text-muted">264</span>
                                </li>
                                <li class="cz-filter-item d-flex justify-content-between align-items-center mb-1">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="wearhouse">
                                        <label class="custom-control-label cz-filter-item-text" for="wearhouse">Men's
                                            Wearhouse</label>
                                    </div>
                                    <span class="font-size-xs text-muted">75</span>
                                </li>
                                <li class="cz-filter-item d-flex justify-content-between align-items-center mb-1">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="newbalance">
                                        <label class="custom-control-label cz-filter-item-text" for="newbalance">New
                                            Balance</label>
                                    </div>
                                    <span class="font-size-xs text-muted">218</span>
                                </li>
                                <li class="cz-filter-item d-flex justify-content-between align-items-center mb-1">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="nike">
                                        <label class="custom-control-label cz-filter-item-text" for="nike">Nike</label>
                                    </div>
                                    <span class="font-size-xs text-muted">810</span>
                                </li>
                                <li class="cz-filter-item d-flex justify-content-between align-items-center mb-1">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="navy">
                                        <label class="custom-control-label cz-filter-item-text" for="navy">Old Navy</label>
                                    </div>
                                    <span class="font-size-xs text-muted">147</span>
                                </li>
                                <li class="cz-filter-item d-flex justify-content-between align-items-center mb-1">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="polo">
                                        <label class="custom-control-label cz-filter-item-text" for="polo">Polo Ralph
                                            Lauren</label>
                                    </div>
                                    <span class="font-size-xs text-muted">64</span>
                                </li>
                                <li class="cz-filter-item d-flex justify-content-between align-items-center mb-1">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="puma">
                                        <label class="custom-control-label cz-filter-item-text" for="puma">Puma</label>
                                    </div>
                                    <span class="font-size-xs text-muted">370</span>
                                </li>
                                <li class="cz-filter-item d-flex justify-content-between align-items-center mb-1">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="reebok">
                                        <label class="custom-control-label cz-filter-item-text" for="reebok">Reebok</label>
                                    </div>
                                    <span class="font-size-xs text-muted">506</span>
                                </li>
                                <li class="cz-filter-item d-flex justify-content-between align-items-center mb-1">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="skechers">
                                        <label class="custom-control-label cz-filter-item-text"
                                               for="skechers">Skechers</label>
                                    </div>
                                    <span class="font-size-xs text-muted">209</span>
                                </li>
                                <li class="cz-filter-item d-flex justify-content-between align-items-center mb-1">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="hilfiger">
                                        <label class="custom-control-label cz-filter-item-text" for="hilfiger">Tommy
                                            Hilfiger</label>
                                    </div>
                                    <span class="font-size-xs text-muted">487</span>
                                </li>
                                <li class="cz-filter-item d-flex justify-content-between align-items-center mb-1">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="armour">
                                        <label class="custom-control-label cz-filter-item-text" for="armour">Under
                                            Armour</label>
                                    </div>
                                    <span class="font-size-xs text-muted">90</span>
                                </li>
                                <li class="cz-filter-item d-flex justify-content-between align-items-center mb-1">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="urban">
                                        <label class="custom-control-label cz-filter-item-text" for="urban">Urban
                                            Outfitters</label>
                                    </div>
                                    <span class="font-size-xs text-muted">152</span>
                                </li>
                                <li class="cz-filter-item d-flex justify-content-between align-items-center mb-1">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="vsecret">
                                        <label class="custom-control-label cz-filter-item-text" for="vsecret">Victoria's
                                            Secret</label>
                                    </div>
                                    <span class="font-size-xs text-muted">238</span>
                                </li>
                                <li class="cz-filter-item d-flex justify-content-between align-items-center mb-1">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="wolverine">
                                        <label class="custom-control-label cz-filter-item-text"
                                               for="wolverine">Wolverine</label>
                                    </div>
                                    <span class="font-size-xs text-muted">29</span>
                                </li>
                                <li class="cz-filter-item d-flex justify-content-between align-items-center">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="wrangler">
                                        <label class="custom-control-label cz-filter-item-text"
                                               for="wrangler">Wrangler</label>
                                    </div>
                                    <span class="font-size-xs text-muted">115</span>
                                </li>
                            </ul>
                        </div>
                        <!-- Filter by Size-->
                        <div class="widget cz-filter mb-4 pb-4 border-bottom">
                            <h3 class="widget-title">Size</h3>
                            <div class="input-group-overlay input-group-sm mb-2">
                                <input class="cz-filter-search form-control form-control-sm appended-form-control"
                                       type="text" placeholder="Search">
                                <div class="input-group-append-overlay"><span class="input-group-text"><i
                                            class="czi-search"></i></span></div>
                            </div>
                            <ul class="widget-list cz-filter-list list-unstyled pt-1" style="max-height: 12rem;"
                                data-simplebar data-simplebar-auto-hide="false">
                                <li class="cz-filter-item d-flex justify-content-between align-items-center mb-1">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="size-xs">
                                        <label class="custom-control-label cz-filter-item-text" for="size-xs">XS</label>
                                    </div>
                                    <span class="font-size-xs text-muted">34</span>
                                </li>
                                <li class="cz-filter-item d-flex justify-content-between align-items-center mb-1">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="size-s">
                                        <label class="custom-control-label cz-filter-item-text" for="size-s">S</label>
                                    </div>
                                    <span class="font-size-xs text-muted">57</span>
                                </li>
                                <li class="cz-filter-item d-flex justify-content-between align-items-center mb-1">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="size-m">
                                        <label class="custom-control-label cz-filter-item-text" for="size-m">M</label>
                                    </div>
                                    <span class="font-size-xs text-muted">198</span>
                                </li>
                                <li class="cz-filter-item d-flex justify-content-between align-items-center mb-1">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="size-l">
                                        <label class="custom-control-label cz-filter-item-text" for="size-l">L</label>
                                    </div>
                                    <span class="font-size-xs text-muted">72</span>
                                </li>
                                <li class="cz-filter-item d-flex justify-content-between align-items-center mb-1">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="size-xl">
                                        <label class="custom-control-label cz-filter-item-text" for="size-xl">XL</label>
                                    </div>
                                    <span class="font-size-xs text-muted">46</span>
                                </li>
                                <li class="cz-filter-item d-flex justify-content-between align-items-center mb-1">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="size-39">
                                        <label class="custom-control-label cz-filter-item-text" for="size-39">39</label>
                                    </div>
                                    <span class="font-size-xs text-muted">112</span>
                                </li>
                                <li class="cz-filter-item d-flex justify-content-between align-items-center mb-1">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="size-40">
                                        <label class="custom-control-label cz-filter-item-text" for="size-40">40</label>
                                    </div>
                                    <span class="font-size-xs text-muted">85</span>
                                </li>
                                <li class="cz-filter-item d-flex justify-content-between align-items-center mb-1">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="size-41">
                                        <label class="custom-control-label cz-filter-item-text" for="size-40">41</label>
                                    </div>
                                    <span class="font-size-xs text-muted">210</span>
                                </li>
                                <li class="cz-filter-item d-flex justify-content-between align-items-center mb-1">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="size-42">
                                        <label class="custom-control-label cz-filter-item-text" for="size-42">42</label>
                                    </div>
                                    <span class="font-size-xs text-muted">57</span>
                                </li>
                                <li class="cz-filter-item d-flex justify-content-between align-items-center mb-1">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="size-43">
                                        <label class="custom-control-label cz-filter-item-text" for="size-43">43</label>
                                    </div>
                                    <span class="font-size-xs text-muted">30</span>
                                </li>
                                <li class="cz-filter-item d-flex justify-content-between align-items-center mb-1">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="size-44">
                                        <label class="custom-control-label cz-filter-item-text" for="size-44">44</label>
                                    </div>
                                    <span class="font-size-xs text-muted">61</span>
                                </li>
                                <li class="cz-filter-item d-flex justify-content-between align-items-center mb-1">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="size-45">
                                        <label class="custom-control-label cz-filter-item-text" for="size-45">45</label>
                                    </div>
                                    <span class="font-size-xs text-muted">23</span>
                                </li>
                                <li class="cz-filter-item d-flex justify-content-between align-items-center mb-1">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="size-46">
                                        <label class="custom-control-label cz-filter-item-text" for="size-46">46</label>
                                    </div>
                                    <span class="font-size-xs text-muted">19</span>
                                </li>
                                <li class="cz-filter-item d-flex justify-content-between align-items-center mb-1">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="size-47">
                                        <label class="custom-control-label cz-filter-item-text" for="size-47">47</label>
                                    </div>
                                    <span class="font-size-xs text-muted">15</span>
                                </li>
                                <li class="cz-filter-item d-flex justify-content-between align-items-center mb-1">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="size-48">
                                        <label class="custom-control-label cz-filter-item-text" for="size-48">48</label>
                                    </div>
                                    <span class="font-size-xs text-muted">12</span>
                                </li>
                                <li class="cz-filter-item d-flex justify-content-between align-items-center mb-1">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="size-49">
                                        <label class="custom-control-label cz-filter-item-text" for="size-49">49</label>
                                    </div>
                                    <span class="font-size-xs text-muted">8</span>
                                </li>
                                <li class="cz-filter-item d-flex justify-content-between align-items-center">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="size-50">
                                        <label class="custom-control-label cz-filter-item-text" for="size-50">50</label>
                                    </div>
                                    <span class="font-size-xs text-muted">6</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </aside>
            <!-- Content  -->
            <section class="col-lg-8">
                <!-- Products grid-->
                <h2 class="text-center pb-5 mt-5  border-bottom">{{$categoryDetails->name}}</h2>

                <div class="row mx-n2 ">
                @foreach($products as $product)
                    <!-- Product-->
                        <div class="col-md-4 col-sm-6 px-2 mb-4">
                            <div class="card product-card">
                                <button class="btn-wishlist btn-sm" type="button" data-toggle="tooltip" data-placement="left"
                                        title="Add to wishlist"><i class="czi-heart"></i></button>
                                <a class="card-img-top d-block overflow-hidden" href="{{url('/product/'.$product->id)}}"><img
                                        src="{{asset('asset/backend/images/products/small/'.$product->image)}} " alt="Product"></a>
                                <div class="card-body py-2"><a class="product-meta d-block font-size-xs pb-1" href="#">Sneakers
                                        &amp; Keds</a>
                                    <h3 class="product-title font-size-sm"><a href="#">{{$product->product_name}}</a></h3>
                                    <div class="d-flex justify-content-between">
                                        <div class="product-price"><span class="text-accent">৳ {{$product->price}}.<small>00</small></span></div>
                                        <div class="star-rating"><i class="sr-star czi-star-filled active"></i><i
                                                class="sr-star czi-star-filled active"></i><i
                                                class="sr-star czi-star-filled active"></i><i
                                                class="sr-star czi-star-filled active"></i><i class="sr-star czi-star"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body card-body-hidden">
                                    <div class="text-center pb-2">
                                        <div class="custom-control custom-option custom-control-inline mb-2">
                                            <input class="custom-control-input" type="radio" name="size1" id="s-75">
                                            <label class="custom-option-label" for="s-75">7.5</label>
                                        </div>
                                        <div class="custom-control custom-option custom-control-inline mb-2">
                                            <input class="custom-control-input" type="radio" name="size1" id="s-80" checked>
                                            <label class="custom-option-label" for="s-80">8</label>
                                        </div>
                                        <div class="custom-control custom-option custom-control-inline mb-2">
                                            <input class="custom-control-input" type="radio" name="size1" id="s-85">
                                            <label class="custom-option-label" for="s-85">8.5</label>
                                        </div>
                                        <div class="custom-control custom-option custom-control-inline mb-2">
                                            <input class="custom-control-input" type="radio" name="size1" id="s-90">
                                            <label class="custom-option-label" for="s-90">9</label>
                                        </div>
                                    </div>
                                    <a href="{{url('/product/'.$product->id)}}">
                                        <button class="btn btn-primary btn-sm btn-block mb-2" type="button" data-toggle="toast"
                                                data-target="#cart-toast"><i class="czi-cart font-size-sm mr-1"></i>Add to Cart
                                        </button>
                                    </a>

                                    <div class="text-center"><a class="nav-link-style font-size-ms" href="#quick-view"
                                                                data-toggle="modal"><i class="czi-eye align-middle mr-1"></i>Quick
                                            view</a></div>
                                </div>
                            </div>
                            <hr class="d-sm-none">
                        </div>
                    @endforeach
                </div>

                <!-- Pagination-->
                <nav class="d-flex justify-content-between pt-2" aria-label="Page navigation">
                    <ul class="pagination">
                        <li class="page-item"><a class="page-link" href="#"><i class="czi-arrow-left mr-2"></i>Prev</a></li>
                    </ul>
                    <ul class="pagination">
                        <li class="page-item d-sm-none"><span class="page-link page-link-static">1 / 5</span></li>
                        <li class="page-item active d-none d-sm-block" aria-current="page"><span class="page-link">1<span
                                    class="sr-only">(current)</span></span>
                        </li>
                        <li class="page-item d-none d-sm-block"><a class="page-link" href="#">2</a></li>
                        <li class="page-item d-none d-sm-block"><a class="page-link" href="#">3</a></li>
                        <li class="page-item d-none d-sm-block"><a class="page-link" href="#">4</a></li>
                        <li class="page-item d-none d-sm-block"><a class="page-link" href="#">5</a></li>
                    </ul>
                    <ul class="pagination">
                        <li class="page-item"><a class="page-link" href="#" aria-label="Next">Next<i
                                    class="czi-arrow-right ml-2"></i></a></li>
                    </ul>
                </nav>
            </section>
        </div>
    </div>
    <!-- Banners-->
    <section class="container pb-4 mb-md-3 ">
        <div class="row ">
            <div class="col-md-8 mb-4 ">
                <div class="d-sm-flex justify-content-between align-items-center bg-secondary overflow-hidden rounded-lg ">
                    <div class="py-4 my-2 my-md-0 py-md-5 px-4 ml-md-3 text-center text-sm-left ">
                        <h4 class="font-size-lg font-weight-light mb-2 ">Hurry up! Limited time offer</h4>
                        <h3 class="mb-4 ">Converse All Star on Sale</h3><a class="btn btn-primary btn-shadow btn-sm "
                                                                           href="# ">Shop Now</a>
                    </div>
                    <img class="d-block ml-auto " src="{{asset('asset/frontend/img/shop/catalog/banner.jpg')}}  " alt="Shop Converse ">
                </div>
            </div>
            <div class="col-md-4 mb-4 ">
                <div class="d-flex flex-column h-100 justify-content-center bg-size-cover bg-position-center rounded-lg "
                     style="background-image: url('{{asset('asset/frontend/img/blog/banner-bg.jpg')}}')">
                    <img class="d-block ml-auto " src="{{asset('asset/frontend/img/shop/catalog/Summer-Sale-2019.png ')}}')}} " alt="Shop Converse ">
                </div>
            </div>
        </div>
    </section>
    <!-- Featured category (Hoodie)-->
    <section class="container mb-4 pb-3 pb-sm-0 mb-sm-5 ">
        <div class="row ">
            <!-- Banner with controls-->
            <div class="col-md-5 ">
                <div class="d-flex flex-column h-100 overflow-hidden rounded-lg " style="background-color: #e2e9ef; ">
                    <div class="d-flex justify-content-between px-grid-gutter py-grid-gutter ">
                        <div>
                            <h3 class="mb-1 ">Hoodie day</h3><a class="font-size-md " href="shop-grid-ls.html ">Shop hoodies<i
                                    class="czi-arrow-right font-size-xs align-middle ml-1 "></i></a>
                        </div>
                        <div class="cz-custom-controls " id="hoodie-day ">
                            <button type="button "><i class="czi-arrow-left "></i></button>
                            <button type="button "><i class="czi-arrow-right "></i></button>
                        </div>
                    </div>
                    <a class="d-none d-md-block mt-auto " href="shop-grid-ls.html "><img class="d-block w-100 "
                                                                                         src="{{asset('asset/frontend/img/home/categories/cat-lg04.jpg')}} "
                                                                                         alt="For Women "></a>
                </div>
            </div>
            <!-- Product grid (carousel)-->
            <div class="col-md-7 pt-4 pt-md-0 ">
                <h2 class="text-center mb-5">Season Collection</h2>
                <div class="cz-carousel ">
                    <div class="cz-carousel-inner "
                         data-carousel-options="{&quot;nav&quot;: false, &quot;controlsContainer&quot;: &quot;#hoodie-day&quot;} ">
                        <!-- Carousel item-->
                        <div>
                            <div class="row mx-n2 ">
                                <div class="col-lg-4 col-6 px-0 px-sm-2 mb-sm-4 ">
                                    <div class="card product-card card-static ">
                                        <button class="btn-wishlist btn-sm " type="button " data-toggle="tooltip "
                                                data-placement="left " title="Add to wishlist "><i class="czi-heart "></i>
                                        </button>
                                        <a class="card-img-top d-block overflow-hidden " href="shop-single-v1.html "><img
                                                src="{{asset('asset/frontend/img/shop/catalog/20.jpg')}}  " alt="Product "></a>
                                        <div class="card-body py-2 "><a class="product-meta d-block font-size-xs pb-1 "
                                                                        href="# ">Hoodies &amp; Sweatshirts</a>
                                            <h3 class="product-title font-size-sm "><a href="shop-single-v1.html ">Block-colored
                                                    Hooded Top</a></h3>
                                            <div class="d-flex justify-content-between ">
                                                <div class="product-price "><span class="text-accent ">$24.<small>99</small></span>
                                                </div>
                                                <div class="star-rating "><i class="sr-star czi-star-filled active "></i><i
                                                        class="sr-star czi-star-filled active "></i><i
                                                        class="sr-star czi-star-filled active "></i><i class="sr-star czi-star-filled
        active "></i><i class="sr-star czi-star "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-6 px-0 px-sm-2 mb-sm-4 ">
                                    <div class="card product-card card-static ">
                                        <button class="btn-wishlist btn-sm " type="button " data-toggle="tooltip "
                                                data-placement="left " title="Add to wishlist "><i class="czi-heart "></i>
                                        </button>
                                        <a class="card-img-top d-block overflow-hidden " href="shop-single-v1.html "><img
                                                src="{{asset('asset/frontend/img/shop/catalog/21.jpg')}}  " alt="Product "></a>
                                        <div class="card-body py-2 "><a class="product-meta d-block font-size-xs pb-1 "
                                                                        href="# ">Hoodies &amp; Sweatshirts</a>
                                            <h3 class="product-title font-size-sm "><a href="shop-single-v1.html ">Block-colored
                                                    Hooded Top</a></h3>
                                            <div class="d-flex justify-content-between ">
                                                <div class="product-price "><span class="text-accent ">$26.<small>99</small></span>
                                                </div>
                                                <div class="star-rating "><i class="sr-star czi-star-filled active "></i><i
                                                        class="sr-star czi-star-filled active "></i><i
                                                        class="sr-star czi-star-filled active "></i><i class="sr-star czi-star-filled
        active "></i><i class="sr-star czi-star-filled active "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-6 px-0 px-sm-2 mb-sm-4 ">
                                    <div class="card product-card card-static "><span
                                            class="badge badge-danger badge-shadow ">Sale</span>
                                        <button class="btn-wishlist btn-sm " type="button " data-toggle="tooltip "
                                                data-placement="left " title="Add to wishlist "><i class="czi-heart "></i>
                                        </button>
                                        <a class="card-img-top d-block overflow-hidden " href="shop-single-v1.html "><img
                                                src="{{asset('asset/frontend/img/shop/catalog/23.jpg')}}  " alt="Product "></a>
                                        <div class="card-body py-2 "><a class="product-meta d-block font-size-xs pb-1 "
                                                                        href="# ">Hoodies &amp; Sweatshirts</a>
                                            <h3 class="product-title font-size-sm "><a href="shop-single-v1.html ">Block-colored
                                                    Hooded Top</a></h3>
                                            <div class="d-flex justify-content-between ">
                                                <div class="product-price "><span class="text-accent ">$17.<small>99</small></span>
                                                    <del class="font-size-sm text-muted ">24.
                                                        <small>99</small>
                                                    </del>
                                                </div>
                                                <div class="star-rating "><i class="sr-star czi-star-filled active "></i><i
                                                        class="sr-star czi-star-filled active "></i><i
                                                        class="sr-star czi-star-filled active "></i><i class="sr-star czi-star
        "></i><i class="sr-star czi-star "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-6 px-0 px-sm-2 mb-sm-4 ">
                                    <div class="card product-card card-static ">
                                        <button class="btn-wishlist btn-sm " type="button " data-toggle="tooltip "
                                                data-placement="left " title="Add to wishlist "><i class="czi-heart "></i>
                                        </button>
                                        <a class="card-img-top d-block overflow-hidden " href="shop-single-v1.html "><img
                                                src="{{asset('asset/frontend/img/shop/catalog/51.jpg')}}  " alt="Product "></a>
                                        <div class="card-body py-2 "><a class="product-meta d-block font-size-xs pb-1 "
                                                                        href="# ">Hoodies &amp; Sweatshirts</a>
                                            <h3 class="product-title font-size-sm "><a href="shop-single-v1.html ">Mono
                                                    Color Hooded Top</a></h3>
                                            <div class="d-flex justify-content-between ">
                                                <div class="product-price "><span class="text-accent ">$21.<small>99</small></span>
                                                </div>
                                                <div class="star-rating "><i class="sr-star czi-star-filled active "></i><i
                                                        class="sr-star czi-star-filled active "></i><i
                                                        class="sr-star czi-star-filled active "></i><i class="sr-star czi-star-filled
        active "></i><i class="sr-star czi-star-filled active "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-6 px-0 px-sm-2 mb-sm-4 d-none d-lg-block ">
                                    <div class="card product-card card-static ">
                                        <button class="btn-wishlist btn-sm " type="button " data-toggle="tooltip "
                                                data-placement="left " title="Add to wishlist "><i class="czi-heart "></i>
                                        </button>
                                        <a class="card-img-top d-block overflow-hidden " href="shop-single-v1.html "><img
                                                src="{{asset('asset/frontend/img/shop/catalog/24.jpg')}}  " alt="Product "></a>
                                        <div class="card-body py-2 "><a class="product-meta d-block font-size-xs pb-1 "
                                                                        href="# ">Hoodies &amp; Sweatshirts</a>
                                            <h3 class="product-title font-size-sm "><a href="shop-single-v1.html ">Block-colored
                                                    Hooded Top</a></h3>
                                            <div class="d-flex justify-content-between ">
                                                <div class="product-price "><span class="text-accent ">$24.<small>99</small></span>
                                                </div>
                                                <div class="star-rating "><i class="sr-star czi-star-filled active "></i><i
                                                        class="sr-star czi-star-filled active "></i><i
                                                        class="sr-star czi-star-filled active "></i><i class="sr-star czi-star-filled
        active "></i><i class="sr-star czi-star "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-6 px-0 px-sm-2 mb-sm-4 d-none d-lg-block ">
                                    <div class="card product-card card-static ">
                                        <button class="btn-wishlist btn-sm " type="button " data-toggle="tooltip "
                                                data-placement="left " title="Add to wishlist "><i class="czi-heart "></i>
                                        </button>
                                        <a class="card-img-top d-block overflow-hidden " href="shop-single-v1.html "><img
                                                src="{{asset('asset/frontend/img/shop/catalog/54.jpg')}}  " alt="Product "></a>
                                        <div class="card-body py-2 "><a class="product-meta d-block font-size-xs pb-1 "
                                                                        href="# ">Hoodies &amp; Sweatshirts</a>
                                            <h3 class="product-title font-size-sm "><a href="shop-single-v1.html ">Mono
                                                    Color Hooded Top</a></h3>
                                            <div class="d-flex justify-content-between ">
                                                <div class="product-price "><span class="text-accent ">$21.<small>99</small></span>
                                                </div>
                                                <div class="star-rating "><i class="sr-star czi-star-filled active "></i><i
                                                        class="sr-star czi-star-filled active "></i><i
                                                        class="sr-star czi-star-filled active "></i><i class="sr-star czi-star-filled
        active "></i><i class="sr-star czi-star "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Carousel item-->
                        <div>
                            <div class="row mx-n2 ">
                                <div class="col-lg-4 col-6 px-0 px-sm-2 mb-sm-4 d-none d-lg-block ">
                                    <div class="card product-card card-static ">
                                        <button class="btn-wishlist btn-sm " type="button " data-toggle="tooltip "
                                                data-placement="left " title="Add to wishlist "><i class="czi-heart "></i>
                                        </button>
                                        <a class="card-img-top d-block overflow-hidden " href="shop-single-v1.html "><img
                                                src="{{asset('asset/frontend/img/shop/catalog/53.jpg')}}  " alt="Product "></a>
                                        <div class="card-body py-2 "><a class="product-meta d-block font-size-xs pb-1 "
                                                                        href="# ">Hoodies &amp; Sweatshirts</a>
                                            <h3 class="product-title font-size-sm "><a href="shop-single-v1.html ">Mono
                                                    Color Hooded Top</a></h3>
                                            <div class="d-flex justify-content-between ">
                                                <div class="product-price "><span class="text-accent ">$21.<small>99</small></span>
                                                </div>
                                                <div class="star-rating "><i class="sr-star czi-star-filled active "></i><i
                                                        class="sr-star czi-star-filled active "></i><i
                                                        class="sr-star czi-star-filled active "></i><i class="sr-star czi-star-filled
        active "></i><i class="sr-star czi-star "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-6 px-0 px-sm-2 mb-sm-4 d-none d-lg-block ">
                                    <div class="card product-card card-static ">
                                        <button class="btn-wishlist btn-sm " type="button " data-toggle="tooltip "
                                                data-placement="left " title="Add to wishlist "><i class="czi-heart "></i>
                                        </button>
                                        <a class="card-img-top d-block overflow-hidden " href="shop-single-v1.html "><img
                                                src="{{asset('asset/frontend/img/shop/catalog/52.jpg')}}  " alt="Product "></a>
                                        <div class="card-body py-2 "><a class="product-meta d-block font-size-xs pb-1 "
                                                                        href="# ">Hoodies &amp; Sweatshirts</a>
                                            <h3 class="product-title font-size-sm "><a href="shop-single-v1.html ">Printed
                                                    Hooded Top</a></h3>
                                            <div class="d-flex justify-content-between ">
                                                <div class="product-price "><span class="text-accent ">$25.<small>99</small></span>
                                                </div>
                                                <div class="star-rating "><i class="sr-star czi-star-filled active "></i><i
                                                        class="sr-star czi-star-filled active "></i><i
                                                        class="sr-star czi-star-filled active "></i><i class="sr-star czi-star-filled
        active "></i><i class="sr-star czi-star "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-6 px-0 px-sm-2 mb-sm-4 ">
                                    <div class="card product-card card-static ">
                                        <button class="btn-wishlist btn-sm " type="button " data-toggle="tooltip "
                                                data-placement="left " title="Add to wishlist "><i class="czi-heart "></i>
                                        </button>
                                        <a class="card-img-top d-block overflow-hidden " href="shop-single-v1.html "><img
                                                src="{{asset('asset/frontend/img/shop/catalog/22.jpg')}}  " alt="Product "></a>
                                        <div class="card-body py-2 "><a class="product-meta d-block font-size-xs pb-1 "
                                                                        href="# ">Hoodies &amp; Sweatshirts</a>
                                            <h3 class="product-title font-size-sm "><a href="shop-single-v1.html ">Block-colored
                                                    Hooded Top</a></h3>
                                            <div class="d-flex justify-content-between ">
                                                <div class="product-price "><span class="text-accent ">$24.<small>99</small></span>
                                                </div>
                                                <div class="star-rating "><i class="sr-star czi-star-filled active "></i><i
                                                        class="sr-star czi-star-filled active "></i><i
                                                        class="sr-star czi-star-filled active "></i><i class="sr-star czi-star-filled
        active "></i><i class="sr-star czi-star-filled active "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-6 px-0 px-sm-2 mb-sm-4 ">
                                    <div class="card product-card card-static ">
                                        <button class="btn-wishlist btn-sm " type="button " data-toggle="tooltip "
                                                data-placement="left " title="Add to wishlist "><i class="czi-heart "></i>
                                        </button>
                                        <a class="card-img-top d-block overflow-hidden " href="shop-single-v1.html "><img
                                                src="{{asset('asset/frontend/img/shop/catalog/56.jpg')}}  " alt="Product "></a>
                                        <div class="card-body py-2 "><a class="product-meta d-block font-size-xs pb-1 "
                                                                        href="# ">Hoodies &amp; Sweatshirts</a>
                                            <h3 class="product-title font-size-sm "><a href="shop-single-v1.html ">Printed
                                                    Hooded Top</a></h3>
                                            <div class="d-flex justify-content-between ">
                                                <div class="product-price "><span class="text-accent ">$25.<small>99</small></span>
                                                </div>
                                                <div class="star-rating "><i class="sr-star czi-star-filled active "></i><i
                                                        class="sr-star czi-star-filled active "></i><i
                                                        class="sr-star czi-star-filled active "></i><i class="sr-star czi-star-filled
        active "></i><i class="sr-star czi-star-filled active "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-6 px-0 px-sm-2 mb-sm-4 ">
                                    <div class="card product-card card-static ">
                                        <button class="btn-wishlist btn-sm " type="button " data-toggle="tooltip "
                                                data-placement="left " title="Add to wishlist "><i class="czi-heart "></i>
                                        </button>
                                        <a class="card-img-top d-block overflow-hidden " href="shop-single-v1.html "><img
                                                src="{{asset('asset/frontend/img/shop/catalog/55.jpg')}}  " alt="Product "></a>
                                        <div class="card-body py-2 "><a class="product-meta d-block font-size-xs pb-1 "
                                                                        href="# ">Hoodies &amp; Sweatshirts</a>
                                            <h3 class="product-title font-size-sm "><a href="shop-single-v1.html ">Block-colored
                                                    Hooded Top</a></h3>
                                            <div class="d-flex justify-content-between ">
                                                <div class="product-price "><span class="text-accent ">$24.<small>99</small></span>
                                                </div>
                                                <div class="star-rating "><i class="sr-star czi-star-filled active "></i><i
                                                        class="sr-star czi-star-filled active "></i><i
                                                        class="sr-star czi-star-filled active "></i><i class="sr-star czi-star-filled
        active "></i><i class="sr-star czi-star "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-6 px-0 px-sm-2 mb-sm-4 ">
                                    <div class="card product-card card-static ">
                                        <button class="btn-wishlist btn-sm " type="button " data-toggle="tooltip "
                                                data-placement="left " title="Add to wishlist "><i class="czi-heart "></i>
                                        </button>
                                        <a class="card-img-top d-block overflow-hidden " href="shop-single-v1.html "><img
                                                src="{{asset('asset/frontend/img/shop/catalog/57.jpg')}}  " alt="Product "></a>
                                        <div class="card-body py-2 "><a class="product-meta d-block font-size-xs pb-1 "
                                                                        href="# ">Hoodies &amp; Sweatshirts</a>
                                            <h3 class="product-title font-size-sm "><a href="shop-single-v1.html ">Block-colored
                                                    Hooded Top</a></h3>
                                            <div class="d-flex justify-content-between ">
                                                <div class="product-price "><span class="text-accent ">$23.<small>99</small></span>
                                                </div>
                                                <div class="star-rating "><i class="sr-star czi-star-filled active "></i><i
                                                        class="sr-star czi-star-filled active "></i><i
                                                        class="sr-star czi-star-filled active "></i><i class="sr-star czi-star
        "></i><i class="sr-star czi-star "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Shop by brand-->
    <section class="container py-lg-4 mb-4 ">
        <h2 class="h3 text-center pb-4 ">Shop by BRAND</h2>
        <div class="row ">
            <div class="col-md-3 col-sm-4 col-6 ">
                <a class="d-block bg-white box-shadow-sm rounded-lg py-3 py-sm-4 mb-grid-gutter " href="# "><img
                        class="d-block mx-auto " src="{{asset('asset/frontend/img/shop/brands/01.png ')}} " style="width: 150px; " alt="Brand "></a>
            </div>
            <div class="col-md-3 col-sm-4 col-6 ">
                <a class="d-block bg-white box-shadow-sm rounded-lg py-3 py-sm-4 mb-grid-gutter " href="# "><img
                        class="d-block mx-auto " src="{{asset('asset/frontend/img/shop/brands/02.png ')}} " style="width: 150px; " alt="Brand "></a>
            </div>
            <div class="col-md-3 col-sm-4 col-6 ">
                <a class="d-block bg-white box-shadow-sm rounded-lg py-3 py-sm-4 mb-grid-gutter " href="# "><img
                        class="d-block mx-auto " src="{{asset('asset/frontend/img/shop/brands/03.png ')}} " style="width: 150px; " alt="Brand "></a>
            </div>
            <div class="col-md-3 col-sm-4 col-6 ">
                <a class="d-block bg-white box-shadow-sm rounded-lg py-3 py-sm-4 mb-grid-gutter " href="# "><img
                        class="d-block mx-auto " src="{{asset('asset/frontend/img/shop/brands/04.png ')}} " style="width: 150px; " alt="Brand "></a>
            </div>
            <div class="col-md-3 col-sm-4 col-6 ">
                <a class="d-block bg-white box-shadow-sm rounded-lg py-3 py-sm-4 mb-grid-gutter " href="# "><img
                        class="d-block mx-auto " src="{{asset('asset/frontend/img/shop/brands/05.png ')}} " style="width: 150px; " alt="Brand "></a>
            </div>
            <div class="col-md-3 col-sm-4 col-6 ">
                <a class="d-block bg-white box-shadow-sm rounded-lg py-3 py-sm-4 mb-grid-gutter " href="# "><img
                        class="d-block mx-auto " src="{{asset('asset/frontend/img/shop/brands/06.png ')}} " style="width: 150px; " alt="Brand "></a>
            </div>
            <div class="col-md-3 col-sm-4 col-6 ">
                <a class="d-block bg-white box-shadow-sm rounded-lg py-3 py-sm-4 mb-grid-gutter " href="# "><img
                        class="d-block mx-auto " src="{{asset('asset/frontend/img/shop/brands/07.png ')}} " style="width: 150px; " alt="Brand "></a>
            </div>
            <div class="col-md-3 col-sm-4 col-6 ">
                <a class="d-block bg-white box-shadow-sm rounded-lg py-3 py-sm-4 mb-grid-gutter " href="# "><img
                        class="d-block mx-auto " src="{{asset('asset/frontend/img/shop/brands/08.png ')}} " style="width: 150px; " alt="Brand "></a>
            </div>
            <div class="col-md-3 col-sm-4 col-6 ">
                <a class="d-block bg-white box-shadow-sm rounded-lg py-3 py-sm-4 mb-grid-gutter " href="# "><img
                        class="d-block mx-auto " src="{{asset('asset/frontend/img/shop/brands/09.png ')}} " style="width: 150px; " alt="Brand "></a>
            </div>
            <div class="col-md-3 col-sm-4 col-6 ">
                <a class="d-block bg-white box-shadow-sm rounded-lg py-3 py-sm-4 mb-grid-gutter " href="# "><img
                        class="d-block mx-auto " src="{{asset('asset/frontend/img/shop/brands/10.png ')}} " style="width: 150px; " alt="Brand "></a>
            </div>
            <div class="col-md-3 col-sm-4 col-6 ">
                <a class="d-block bg-white box-shadow-sm rounded-lg py-3 py-sm-4 mb-grid-gutter " href="# "><img
                        class="d-block mx-auto " src="{{asset('asset/frontend/img/shop/brands/11.png ')}} " style="width: 150px; " alt="Brand "></a>
            </div>
            <div class="col-md-3 col-sm-4 col-6 ">
                <a class="d-block bg-white box-shadow-sm rounded-lg py-3 py-sm-4 mb-grid-gutter " href="# "><img
                        class="d-block mx-auto " src="{{asset('asset/frontend/img/shop/brands/12.png ')}}')}} " style="width: 150px; " alt="Brand "></a>
            </div>
        </div>
    </section>
    <!-- Blog + Instagram info cards-->
    <section class="container-fluid px-0 ">
        <div class="row no-gutters ">
            <div class="col-md-6 ">
                <a class="card border-0 rounded-0 text-decoration-none py-md-4 bg-faded-primary "
                   href="blog-list-sidebar.html ">
                    <div class="card-body text-center "><i class="czi-edit h3 mt-2 mb-4 text-primary "></i>
                        <h3 class="h5 mb-1 ">Read the blog</h3>
                        <p class="text-muted font-size-sm ">Latest store, fashion news and trends</p>
                    </div>
                </a>
            </div>
            <div class="col-md-6 ">
                <a class="card border-0 rounded-0 text-decoration-none py-md-4 bg-faded-accent " href="# ">
                    <div class="card-body text-center "><i class="czi-instagram h3 mt-2 mb-4 text-accent "></i>
                        <h3 class="h5 mb-1 ">Follow on Instagram</h3>
                        <p class="text-muted font-size-sm ">#ShopWithCholoKini</p>
                    </div>
                </a>
            </div>
        </div>
    </section>
    <!-- Toast: Added to Cart-->
    <div class="toast-container toast-bottom-center ">
        <div class="toast mb-3 " id="cart-toast " data-delay="5000 " role="alert " aria-live="assertive "
             aria-atomic="true ">
            <div class="toast-header bg-success text-white "><i class="czi-check-circle mr-2 "></i>
                <h6 class="font-size-sm text-white mb-0 mr-auto ">Added to cart!</h6>
                <button class="close text-white ml-2 mb-1 " type="button " data-dismiss="toast " aria-label="Close "><span
                        aria-hidden="true ">&times;</span></button>
            </div>
            <div class="toast-body ">This item has been added to your cart.</div>
        </div>
    </div>
@stop
