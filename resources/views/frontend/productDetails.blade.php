@extends('layouts.frontend.frontendDesign')
@section('content')
    <!-- Page Title (Shop)-->
    <div class="page-title-overlap bg-dark pt-4">
        <div class="container d-lg-flex justify-content-between py-2 py-lg-3">
            <div class="order-lg-2 mb-3 mb-lg-0 pt-lg-2">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-light flex-lg-nowrap justify-content-center justify-content-lg-star">
                        <li class="breadcrumb-item"><a class="text-nowrap" href="index.html"><i class="czi-home"></i>Home</a></li>
                        <li class="breadcrumb-item text-nowrap"><a href="#">Shop</a>
                        </li>
                        <li class="breadcrumb-item text-nowrap active" aria-current="page">{{$productDetails->product_name}} - Details</li>
                    </ol>
                </nav>
            </div>
            <div class="order-lg-1 pr-lg-4 text-center text-lg-left">
                <h1 class="h3 text-light mb-0 border-bottom">{{$productDetails->product_name}}</h1>
            </div>
        </div>
    </div>
    <!-- Page Content-->
    <div class="container">
        <!-- Gallery + details-->
        <div class="bg-light box-shadow-lg rounded-lg px-4 py-3 mb-5">
            <div class="px-lg-3">
                @if (Session::get('error'))
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{!! session('error') !!}</strong>
                    </div>
                @endif
                <div class="row">
                    <!-- Product gallery-->


                    <div class="col-lg-7 pr-lg-0 pt-lg-4">
                        <div class="cz-product-gallery">
                            <div class="cz-preview order-sm-2">
                                <div class=" cz-preview-item active " >
                                    <div class="easyzoom easyzoom--overlay easyzoom--with-thumbnails">
                                        <a href="{{asset('asset/backend/images/products/large/'.$productDetails->image)}}">
                                            <img  class="mainImage" src="{{asset('asset/backend/images/products/large/'.$productDetails->image)}}" alt="" />
                                        </a>
                                    </div>
{{--                                    <div class="cz-image-zoom-pane"></div>--}}
                                </div>
                            </div>
{{--                            class="changeImage         style=" display: block; cursor:pointer;border: 1px solid #e3e9ef; width: 5rem; height: 5rem; margin: .625rem; border-radius: .3125rem;  "  --}}
                            <div class="cz-thumblist order-sm-1 thumbnails">
                                <h6 class="border-bottom">Thumbnails:-</h6>
                                @foreach($proAltImages as $proAltImage)
                                    <a href="{{asset('asset/backend/images/products/large/'.$proAltImage->image)}}" data-standard="{{asset('asset/backend/images/products/small/'.$proAltImage->image)}}">
                                        <img class="changeImage" style=" display: block; cursor:pointer;border: 1px solid #e3e9ef; width: 5rem; height: 5rem; margin: .625rem; border-radius: .3125rem;" src="{{asset('asset/backend/images/products/small/'.$proAltImage->image)}}" alt="" />
                                    </a>
                                @endforeach
                            </div>
                        </div>
                    </div>

                    <!-- Product details-->
                    <div class="col-lg-5 pt-4 pt-lg-0">
                        <div class="product-details ml-auto pb-3">
                            <span class="badge badge-danger badge-shadow align-middle mt-n2">New</span>
                            <div class="d-flex justify-content-between align-items-center mb-2">
                                <a href="#reviews" data-scroll>
                                    <div class="star-rating"><i class="sr-star czi-star-filled active"></i><i class="sr-star czi-star-filled active"></i><i class="sr-star czi-star-filled active"></i><i class="sr-star czi-star-filled active"></i><i class="sr-star czi-star"></i>
                                    </div><span class="d-inline-block font-size-sm text-body align-middle mt-1 ml-1">74 Reviews</span></a>
                                <button class="btn-wishlist mr-0 mr-lg-n3" type="button" data-toggle="tooltip" title="Add to wishlist"><i class="czi-heart"></i></button>
                            </div>
                            <div class="font-size-sm mb-4"><span class="text-heading font-weight-medium mr-1">Code :</span><span class="text-muted">{{$productDetails->product_code}}</span></div>
                            <div class="font-size-sm mb-4"><span class="text-heading font-weight-medium mr-1">Color :</span><span class="text-muted">{{$productDetails->product_color}}</span></div>
                            <div class="position-relative mr-n4 mb-3">
                                <div class="mb-3"><span id="getPrice" class="h3 font-weight-normal text-accent mr-1">৳ {{$productDetails->price}}.<small>00</small></span>
                                    <del class="text-muted font-size-lg mr-3">Pre Price.<small>00</small></del>
                                </div>

                                <div class="product-badge product-available mt-n1"><i class="czi-security-check"></i>

                                    <span id="Availability">
                                        @if($totalStock>0)
                                            Product available
                                        @else
                                            Product Out of Stock
                                        @endif
                                    </span>
                                </div>
                            </div>
                            <form class="mb-grid-gutter" method="post" name="addToCartForm" id="addToCartForm" action="{{url('addToCart')}}">
                                {{csrf_field()}}
                                <input type="hidden" name="product_id" value="{{$productDetails->id}}">
                                <input type="hidden" name="product_name" value="{{$productDetails->product_name}}">
                                <input type="hidden" name="product_code" value="{{$productDetails->product_code}}">
                                <input type="hidden" name="product_color" value="{{$productDetails->product_color}}">
                                <input type="hidden" name="price" id="price" value="{{$productDetails->price}}">
                                <div class="form-group">
                                    <div class="d-flex justify-content-between align-items-center pb-1">
                                        <label class="font-weight-medium" for="productSize">Size:</label>
                                    </div>
                                    <select name="size" class="custom-select" required id="productSize">
                                        <option value="" disabled selected>Select size</option>
                                        @foreach($productDetails->productattribute as $sizes)
                                            <option value="{{$productDetails->id}}-{{$sizes->size}}">{{$sizes->size}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group d-flex align-items-center">
{{--                                    <select class="custom-select mr-3" style="width: 5rem;">--}}
{{--                                        <option value="1">1</option>--}}
{{--                                        <option value="2">2</option>--}}
{{--                                        <option value="3">3</option>--}}
{{--                                        <option value="4">4</option>--}}
{{--                                        <option value="5">5</option>--}}
{{--                                    </select>--}}
                                    <label class="font-weight-medium"  for="productSize">Quantity:-</label>
                                    <input class="mr-3 custom-select" style="width: 5rem;" type="text" name="quantity" value="1" />
                                    @if($totalStock>0)
                                        <button class="btn btn-primary btn-shadow btn-block" type="submit" id="cartButton"><i class="czi-cart font-size-lg mr-2"></i>Add to Cart</button>
                                    @endif
                                </div>
                            </form>
                            <!-- Product panels-->
                            <div class="accordion mb-4" id="productPanels">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="accordion-heading"><a href="#productInfo" role="button" data-toggle="collapse" aria-expanded="true" aria-controls="productInfo"><i class="czi-announcement text-muted font-size-lg align-middle mt-n1 mr-2"></i>Product info<span class="accordion-indicator"><i data-feather="chevron-up"></i></span></a></h3>
                                    </div>
                                    <div class="collapse show" id="productInfo" data-parent="#productPanels">
                                        <div class="card-body">
                                            <h6 class="font-size-sm mb-2">Composition</h6>
                                            <ul class="font-size-sm pl-4">
                                                <li>{{$productDetails->description}}</li>
                                                <li>{{$productDetails->care}}</li>
                                            </ul>
                                            <h6 class="font-size-sm mb-2">Art. No.</h6>
                                            <ul class="font-size-sm pl-4 mb-0">
                                                <li>{{$productDetails->product_code}}</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="accordion-heading"><a class="collapsed" href="#shippingOptions" role="button" data-toggle="collapse" aria-expanded="true" aria-controls="shippingOptions"><i class="czi-delivery text-muted lead align-middle mt-n1 mr-2"></i>Shipping options<span class="accordion-indicator"><i data-feather="chevron-up"></i></span></a></h3>
                                    </div>
                                    <div class="collapse" id="shippingOptions" data-parent="#productPanels">
                                        <div class="card-body font-size-sm">
                                            <div class="font-weight-semibold text-dark "><h6 class="text-center">Cash On Delvery*</h6></div>
                                            <div class="d-flex justify-content-between mt-3  pb-2">
                                                <div>
                                                    <div class="font-weight-semibold text-dark">Inside Dhaka </div>
                                                    <div class="font-size-sm text-muted">(2 - 3 days)</div>
                                                </div>
                                                <div><span class="h3 font-weight-normal text-accent mr-1">৳ 50.<small>00</small></span></div>
                                            </div>
                                            <div class="d-flex justify-content-between mt-3 pb-2">
                                                <div>
                                                    <div class="font-weight-semibold text-dark">Outside Dhaka </div>
                                                    <div class="font-size-sm text-muted">(2 - 5 days)</div>
                                                </div>
                                                <div><span class="h3 font-weight-normal text-accent mr-1">৳ 100.<small>00</small></span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Sharing-->
                            <h6 class="d-inline-block align-middle font-size-base my-2 mr-2">Share:</h6><a class="share-btn sb-twitter mr-2 my-2" href="#"><i class="czi-twitter"></i>Twitter</a><a class="share-btn sb-instagram mr-2 my-2" href="#"><i class="czi-instagram"></i>Instagram</a><a class="share-btn sb-facebook my-2"
                                                                                                                                                                                                                                                                                                   href="#"><i class="czi-facebook"></i>Facebook</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Reviews-->
    <div class="border-top border-bottom my-lg-3 py-5">
        <div class="container pt-md-2" id="reviews">
            <div class="row pb-3">
                <div class="col-lg-4 col-md-5">
                    <h2 class="h3 mb-4">74 Reviews</h2>
                    <div class="star-rating mr-2"><i class="czi-star-filled font-size-sm text-accent mr-1"></i><i class="czi-star-filled font-size-sm text-accent mr-1"></i><i class="czi-star-filled font-size-sm text-accent mr-1"></i><i class="czi-star-filled font-size-sm text-accent mr-1"></i>
                        <i class="czi-star font-size-sm text-muted mr-1"></i>
                    </div><span class="d-inline-block align-middle">4.1 Overall rating</span>
                    <p class="pt-3 font-size-sm text-muted">58 out of 74 (77%)<br>Customers recommended this product</p>
                </div>
                <div class="col-lg-8 col-md-7">
                    <div class="d-flex align-items-center mb-2">
                        <div class="text-nowrap mr-3"><span class="d-inline-block align-middle text-muted">5</span><i class="czi-star-filled font-size-xs ml-1"></i></div>
                        <div class="w-100">
                            <div class="progress" style="height: 4px;">
                                <div class="progress-bar bg-success" role="progressbar" style="width: 60%;" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div><span class="text-muted ml-3">43</span>
                    </div>
                    <div class="d-flex align-items-center mb-2">
                        <div class="text-nowrap mr-3"><span class="d-inline-block align-middle text-muted">4</span><i class="czi-star-filled font-size-xs ml-1"></i></div>
                        <div class="w-100">
                            <div class="progress" style="height: 4px;">
                                <div class="progress-bar" role="progressbar" style="width: 27%; background-color: #a7e453;" aria-valuenow="27" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div><span class="text-muted ml-3">16</span>
                    </div>
                    <div class="d-flex align-items-center mb-2">
                        <div class="text-nowrap mr-3"><span class="d-inline-block align-middle text-muted">3</span><i class="czi-star-filled font-size-xs ml-1"></i></div>
                        <div class="w-100">
                            <div class="progress" style="height: 4px;">
                                <div class="progress-bar" role="progressbar" style="width: 17%; background-color: #ffda75;" aria-valuenow="17" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div><span class="text-muted ml-3">9</span>
                    </div>
                    <div class="d-flex align-items-center mb-2">
                        <div class="text-nowrap mr-3"><span class="d-inline-block align-middle text-muted">2</span><i class="czi-star-filled font-size-xs ml-1"></i></div>
                        <div class="w-100">
                            <div class="progress" style="height: 4px;">
                                <div class="progress-bar" role="progressbar" style="width: 9%; background-color: #fea569;" aria-valuenow="9" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div><span class="text-muted ml-3">4</span>
                    </div>
                    <div class="d-flex align-items-center">
                        <div class="text-nowrap mr-3"><span class="d-inline-block align-middle text-muted">1</span><i class="czi-star-filled font-size-xs ml-1"></i></div>
                        <div class="w-100">
                            <div class="progress" style="height: 4px;">
                                <div class="progress-bar bg-danger" role="progressbar" style="width: 4%;" aria-valuenow="4" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div><span class="text-muted ml-3">2</span>
                    </div>
                </div>
            </div>
            <hr class="mt-4 pb-4 mb-3">
        </div>
    </div>
    <!-- Product carousel (Style with)-->
    <div class="container pt-5">
        <h2 class="h3 text-center pb-4 border-bottom">Customers who viewed this item also viewed</h2>
        <div class="carousel slide cz-controls-static cz-controls-outside">
            <div class="carousel-inner" >
                <!-- Product-->
                <?php $count=1; ?>
                @foreach ($relatedProducts->chunk(3) as $chunk)
                    <div <?php if ($count==1){?> class="carousel-item active "  <?php } else{ ?>  <?php } ?> >
                        @foreach ($chunk as $item)
                            <div class=" col-md-3 d-inline-block">
                                <div class="card product-card card-static">
                                    <button class="btn-wishlist btn-sm" type="button" data-toggle="tooltip" data-placement="left" title="Add to wishlist"><i class="czi-heart"></i></button>
                                    <a class="card-img-top d-block overflow-hidden" href="#"><img src="{{asset('asset/backend/images/products/small/'.$item->image)}}" alt="Product"></a>
                                    <div class="card-body py-2"><a class="product-meta d-block font-size-xs pb-1" href="#">Men’s Jeans</a>
                                        <h3 class="product-title font-size-sm"><a href="#">{{ $item->product_name}}</a></h3>
                                        <div class="d-flex justify-content-between">
                                            <div class="product-price"><span class="text-accent">৳ {{$item->price}}.<small>99</small></span></div>
                                            <div class="star-rating"><i class="sr-star czi-star-filled active"></i><i class="sr-star czi-star-filled active"></i><i class="sr-star czi-star-filled active"></i><i class="sr-star czi-star-filled active"></i><i class="sr-star czi-star"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <?php $count++; ?>
                @endforeach
            </div>
        </div>
    </div>
    <!-- Product carousel (You may also like)-->
    <div class="container py-5 my-md-3">
        <h2 class="h3 text-center pb-4">You may also like</h2>
        <div class="cz-carousel cz-controls-static cz-controls-outside">
            <div class="cz-carousel-inner" data-carousel-options="{&quot;items&quot;: 2, &quot;controls&quot;: true, &quot;nav&quot;: false, &quot;autoHeight&quot;: true, &quot;responsive&quot;: {&quot;0&quot;:{&quot;items&quot;:1},&quot;500&quot;:{&quot;items&quot;:2, &quot;gutter&quot;: 18},&quot;768&quot;:{&quot;items&quot;:3, &quot;gutter&quot;: 20}, &quot;1100&quot;:{&quot;items&quot;:4, &quot;gutter&quot;: 30}}}">
                <!-- Product-->
                <div>
                    <div class="card product-card card-static">
                        <button class="btn-wishlist btn-sm" type="button" data-toggle="tooltip" data-placement="left" title="Add to wishlist"><i class="czi-heart"></i></button>
                        <a class="card-img-top d-block overflow-hidden" href="#"><img src="img/shop/catalog/20.jpg" alt="Product"></a>
                        <div class="card-body py-2"><a class="product-meta d-block font-size-xs pb-1" href="#">Men’s Hoodie</a>
                            <h3 class="product-title font-size-sm"><a href="#">Block-colored Hooded Top</a></h3>
                            <div class="d-flex justify-content-between">
                                <div class="product-price"><span class="text-accent">$24.<small>99</small></span></div>
                                <div class="star-rating"><i class="sr-star czi-star-filled active"></i><i class="sr-star czi-star-filled active"></i><i class="sr-star czi-star-filled active"></i><i class="sr-star czi-star-filled active"></i><i class="sr-star czi-star"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Product-->
                <div>
                    <div class="card product-card card-static">
                        <button class="btn-wishlist btn-sm" type="button" data-toggle="tooltip" data-placement="left" title="Add to wishlist"><i class="czi-heart"></i></button>
                        <a class="card-img-top d-block overflow-hidden" href="#"><img src="img/shop/catalog/21.jpg" alt="Product"></a>
                        <div class="card-body py-2"><a class="product-meta d-block font-size-xs pb-1" href="#">Men’s Hoodie</a>
                            <h3 class="product-title font-size-sm"><a href="#">Block-colored Hooded Top</a></h3>
                            <div class="d-flex justify-content-between">
                                <div class="product-price text-accent">$26.<small>99</small></div>
                                <div class="star-rating"><i class="sr-star czi-star-filled active"></i><i class="sr-star czi-star-filled active"></i><i class="sr-star czi-star-filled active"></i><i class="sr-star czi-star-filled active"></i><i class="sr-star czi-star-filled active"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Product-->
                <div>
                    <div class="card product-card card-static">
                        <button class="btn-wishlist btn-sm" type="button" data-toggle="tooltip" data-placement="left" title="Add to wishlist"><i class="czi-heart"></i></button>
                        <a class="card-img-top d-block overflow-hidden" href="#"><img src="img/shop/catalog/22.jpg" alt="Product"></a>
                        <div class="card-body py-2"><a class="product-meta d-block font-size-xs pb-1" href="#">Men’s Hoodie</a>
                            <h3 class="product-title font-size-sm"><a href="#">Block-colored Hooded Top</a></h3>
                            <div class="d-flex justify-content-between">
                                <div class="product-price text-accent">$24.<small>99</small></div>
                                <div class="star-rating"><i class="sr-star czi-star-filled active"></i><i class="sr-star czi-star-filled active"></i><i class="sr-star czi-star-filled active"></i><i class="sr-star czi-star"></i><i class="sr-star czi-star"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Product-->
                <div>
                    <div class="card product-card card-static">
                        <button class="btn-wishlist btn-sm" type="button" data-toggle="tooltip" data-placement="left" title="Add to wishlist"><i class="czi-heart"></i></button>
                        <a class="card-img-top d-block overflow-hidden" href="#"><img src="img/shop/catalog/23.jpg" alt="Product"></a>
                        <div class="card-body py-2"><a class="product-meta d-block font-size-xs pb-1" href="#">Men’s Hoodie</a>
                            <h3 class="product-title font-size-sm"><a href="#">Block-colored Hooded Top</a></h3>
                            <div class="d-flex justify-content-between">
                                <div class="product-price text-accent">$24.<small>99</small></div>
                                <div class="star-rating"><i class="sr-star czi-star-filled active"></i><i class="sr-star czi-star-filled active"></i><i class="sr-star czi-star-filled active"></i><i class="sr-star czi-star-filled active"></i><i class="sr-star czi-star"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Product-->
                <div>
                    <div class="card product-card card-static">
                        <button class="btn-wishlist btn-sm" type="button" data-toggle="tooltip" data-placement="left" title="Add to wishlist"><i class="czi-heart"></i></button>
                        <a class="card-img-top d-block overflow-hidden" href="#"><img src="img/shop/catalog/24.jpg" alt="Product"></a>
                        <div class="card-body py-2"><a class="product-meta d-block font-size-xs pb-1" href="#">Men’s Hoodie</a>
                            <h3 class="product-title font-size-sm"><a href="#">Block-colored Hooded Top</a></h3>
                            <div class="d-flex justify-content-between">
                                <div class="product-price text-accent">$25.<small>00</small></div>
                                <div class="star-rating"><i class="sr-star czi-star-filled active"></i><i class="sr-star czi-star-filled active"></i><i class="sr-star czi-star-filled active"></i><i class="sr-star czi-star"></i><i class="sr-star czi-star"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
