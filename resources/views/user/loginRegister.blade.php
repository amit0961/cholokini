@extends('layouts.frontend.frontendDesign')
@section('content')

<div class="container py-4 py-lg-5 my-4">
    @if (Session::get('error'))
        <div class="text-center alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong> <u>{!! session('error') !!} </u> </strong>
        </div>
    @endif
    @if (Session::get('success'))
        <div class="text-center alert alert-primary alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong> <u>{!! session('success') !!} </u></strong>
        </div>
    @endif
    <div class="row">
        <div class="col-md-6">
            <div class="card border-0 box-shadow">
                <div class="card-body">
                    <h2 class="h4 mb-1 shadow">Log in  <i class="czi-sign-in"></i> </h2>
                    <div class="py-3">
                        <h3 class="d-inline-block align-middle font-size-base font-weight-semibold mb-2 mr-2">With social account:</h3>
                        <div class="d-inline-block align-middle"><a class="social-btn sb-google mr-2 mb-2" href="{{url('/login/google')}}" data-toggle="tooltip" title="Sign in with Google"><i class="czi-google"></i></a>
                            <a class="social-btn sb-facebook mr-2 mb-2" href="#" data-toggle="tooltip" title="Sign in with Facebook"><i class="czi-facebook"></i></a>
                            <a class="social-btn sb-twitter mr-2 mb-2" href="#" data-toggle="tooltip" title="Sign in with Twitter"><i class="czi-twitter"></i></a>
                        </div>
                    </div>
                    <hr>
                    <h3 class="font-size-base pt-4 pb-2">Or using form below</h3>
                    <form class="needs-validation" name="loginForm" id="loginForm" action="{{url('/userLogin')}}" method="post" novalidate>
                        {{csrf_field()}}
                        <div class="input-group-overlay form-group mb-3">
                            <label for="reg-email">E-mail Address <i class="czi-mail"></i></label>
                            <input class="form-control" name="email" type="email" required id="reg-email" placeholder="Enter the valid email here...">
                            <div class="invalid-feedback">Please enter valid email address!</div>
                        </div>
                        <div class="input-group-overlay form-group mt-4">
                            <label for="si-password">Password <i class="czi-security-check"></i></label>
                            <div class="password-toggle">
                                <input class="form-control" name="password" type="password" id="si-password" placeholder="Enter the correct password to access..." required>
                                <label class="password-toggle-btn">
                                    <input class="custom-control-input" type="checkbox"><i class="czi-eye password-toggle-indicator"></i><span class="sr-only">Show password</span>
                                    <div class="invalid-feedback">Please enter valid password!</div>
                                </label>
                            </div>
                        </div>
                        <div class="text-center pt-4 mt-5">
                            <button class="btn btn-primary" type="submit"><i class="czi-sign-in mr-2 ml-n21"></i>Log In</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-6 pt-4 mt-3 mt-md-0">
            <h2 class="h4 mb-3 shadow">No account? Sign up <i class="czi-arrow-down-circle"></i></h2>
            <p class="font-size-sm text-muted mb-4">Registration takes less than a minute but gives you full control over your orders.</p>
            <form action=" {{ url('/userRegister') }} " method="post" class="needs-validation" id="registerForm" name="registerForm" novalidate >
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-sm-8">
                        <div class="form-group">
                            <label for="reg-fn">User Name <i class="czi-user"></i></label>
                            <input class="form-control" name="name"  type="text" required id="reg-fn">
                            <div class="invalid-feedback">Please enter your user name!</div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="reg-email">E-mail Address <i class="czi-mail"></i></label>
                            <input class="form-control" name="email" type="email" required id="reg-email">
                            <div class="invalid-feedback">Please enter valid email address!</div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="si-password">Password <i class="czi-security-check"></i></label>
                            <div class="password-toggle">
                                <input class="form-control" name="password" type="password" id="si-password" required>
                                <label class="password-toggle-btn">
                              <input class="custom-control-input" type="checkbox"><i class="czi-eye password-toggle-indicator"></i><span class="sr-only">Show password</span>
                                    <div class="invalid-feedback">Please enter valid password!</div>
                            </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-center">
                    <button class="btn btn-primary" type="submit"><i class="czi-user mr-2 ml-n1"></i>Sign Up</button>
                </div>
            </form>
        </div>
    </div>
</div>

@stop
