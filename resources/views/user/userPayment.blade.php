@extends('layouts.frontend.frontendDesign')
@section('content')
    <div class="page-title-overlap bg-dark pt-4">
        <div class="container d-lg-flex justify-content-between py-2 py-lg-3">
            <div class="order-lg-2 mb-3 mb-lg-0 pt-lg-2">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-light flex-lg-nowrap justify-content-center justify-content-lg-star">
                        <li class="breadcrumb-item"><a class="text-nowrap" href="index.html"><i class="czi-home"></i>Home</a></li>
                        <li class="breadcrumb-item text-nowrap"><a href="#">Account</a>
                        </li>
                        <li class="breadcrumb-item text-nowrap active" aria-current="page">Dashboard</li>
                    </ol>
                </nav>
            </div>
            <div class="order-lg-1 pr-lg-4 text-center text-lg-left">
                <h1 class="h3 text-light mb-0">Dashboard</h1>
            </div>
        </div>
    </div>
    <!-- Page Content-->
    <div class="container pb-5 mb-2 mb-md-3">
        <div class="row">
            <!-- Sidebar-->
            <aside class="col-lg-4 pt-4 pt-lg-0">
                <div class="cz-sidebar-static rounded-lg box-shadow-lg px-0 pb-0 mb-5 mb-lg-0">
                    <div class="px-4 mb-4">
                        <div class="media align-items-center">
                            <div class="img-thumbnail rounded-circle position-relative" style="width: 6.375rem;"><span class="badge badge-warning" data-toggle="tooltip" title="Reward points">384</span><img class="rounded-circle" src="{{asset('asset/frontend/img/amit.jpg')}}" alt="Amit Saha"></div>
                            <div class="media-body pl-3">
                                <h3 class="font-size-base mb-0">Amit Saha</h3><span class="text-accent font-size-sm">amitsaha5bd@gmail.com</span>
                            </div>
                        </div>
                    </div>
                    <div class="bg-secondary px-4 py-3">
                        <h3 class="font-size-sm mb-0 text-muted">Account settings</h3>
                    </div>
                    <ul class="list-unstyled mb-0">
                        <li class="border-bottom mb-0"><a class="nav-link-style d-flex align-items-center px-4 py-3 active" href="{{url('/userAccount')}}"><i class="czi-basket opacity-60 mr-2"></i>Dashboard</a></li>
                        <li class="border-bottom mb-0"><a class="nav-link-style d-flex align-items-center px-4 py-3 " href="{{url('/userProfile')}}"><i class="czi-user opacity-60 mr-2"></i>Profile info</a></li>
                        <li class="border-bottom mb-0"><a class="nav-link-style d-flex align-items-center px-4 py-3" href="{{url('/userAddress')}}"><i class="czi-location opacity-60 mr-2"></i>Addresses</a></li>
                        <li class="mb-0"><a class="nav-link-style d-flex align-items-center px-4 py-3" href="{{url('/userPayment')}}"><i class="czi-card opacity-60 mr-2"></i>Payment methods</a></li>
                    </ul>
                </div>
            </aside>
            <!-- Content  -->
            <section class="col-lg-8">
                <!-- Toolbar-->
                <div class="d-none d-lg-flex justify-content-between align-items-center pt-lg-3 pb-4 pb-lg-5 mb-lg-4">
                    <h6 class="font-size-base text-light mb-0">Primary payment method is used by default</h6><a class="btn btn-primary btn-sm" href="account-signin.html"><i class="czi-sign-out mr-2"></i>Sign out</a>
                </div>
                <!-- Payment methods list-->
                <div class="table-responsive font-size-md">
                    <table class="table table-hover mb-0">
                        <thead>
                        <tr>
                            <th>Your credit / debit cards</th>
                            <th>Name on card</th>
                            <th>Expires on</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="py-3 align-middle">
                                <div class="media align-items-center"><img class="mr-2" src="{{asset('asset/frontend/img/card-visa.png')}}" width="39" alt="Visa">
                                    <div class="media-body"><span class="font-weight-medium text-heading mr-1">Visa</span>ending in 4999<span class="align-middle badge badge-info ml-2">Primary</span></div>
                                </div>
                            </td>
                            <td class="py-3 align-middle">Amit Saha</td>
                            <td class="py-3 align-middle">08 / 2019</td>
                        </tr>
                        <tr>
                            <td class="py-3 align-middle">
                                <div class="media align-items-center"><img class="mr-2" src="{{asset('asset/frontend/img/card-master.png')}}" width="39" alt="MesterCard">
                                    <div class="media-body"><span class="font-weight-medium text-heading mr-1">MasterCard</span>ending in 0015</div>
                                </div>
                            </td>
                            <td class="py-3 align-middle">Amit Saha</td>
                            <td class="py-3 align-middle">11 / 2021</td>

                        </tr>
                        <tr>
                            <td class="py-3 align-middle">
                                <div class="media align-items-center"><img class="mr-2" src="{{asset('asset/frontend/img/card-paypal.png')}}" width="39" alt="PayPal">
                                    <div class="media-body"><span class="font-weight-medium text-heading mr-1">PayPal</span>amitbusiness@paypal.com</div>
                                </div>
                            </td>
                            <td class="py-3 align-middle">&mdash;</td>
                            <td class="py-3 align-middle">&mdash;</td>

                        </tr>
                        <tr>
                            <td class="py-3 align-middle">
                                <div class="media align-items-center"><img class="mr-2" src="{{asset('asset/frontend/img/card-visa.png')}}" width="39" alt="Visa">
                                    <div class="media-body"><span class="font-weight-medium text-heading mr-1">Visa</span>ending in 6073</div>
                                </div>
                            </td>
                            <td class="py-3 align-middle">Amit Saha</td>
                            <td class="py-3 align-middle">09 / 2021</td>

                        </tr>
                        <tr>
                            <td class="py-3 align-middle">
                                <div class="media align-items-center"><img class="mr-2" src="{{asset('asset/frontend/img/card-visa.png')}}" width="39" alt="Visa">
                                    <div class="media-body"><span class="font-weight-medium text-heading mr-1">Visa</span>ending in 9791</div>
                                </div>
                            </td>
                            <td class="py-3 align-middle">Amit Saha</td>
                            <td class="py-3 align-middle">05 / 2021</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <hr class="pb-4">
            </section>
        </div>
    </div>
@stop
