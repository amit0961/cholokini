@extends('layouts.frontend.frontendDesign')
@section('content')
    <div class="page-title-overlap bg-dark pt-4">
        <div class="container d-lg-flex justify-content-between py-2 py-lg-3">
            <div class="order-lg-2 mb-3 mb-lg-0 pt-lg-2">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-light flex-lg-nowrap justify-content-center justify-content-lg-star">
                        <li class="breadcrumb-item"><a class="text-nowrap" href="index.html"><i class="czi-home"></i>Home</a></li>
                        <li class="breadcrumb-item text-nowrap"><a href="#">Account</a>
                        </li>
                        <li class="breadcrumb-item text-nowrap active" aria-current="page">Dashboard</li>
                    </ol>
                </nav>
            </div>
            <div class="order-lg-1 pr-lg-4 text-center text-lg-left">
                <h1 class="h3 text-light mb-0">Dashboard</h1>
            </div>
        </div>
    </div>
    <!-- Page Content-->
    <div class="container pb-5 mb-2 mb-md-3">
        <div class="row">
            <!-- Sidebar-->
            <aside class="col-lg-4 pt-4 pt-lg-0">
                <div class="cz-sidebar-static rounded-lg box-shadow-lg px-0 pb-0 mb-5 mb-lg-0">
                    <div class="px-4 mb-4">
                        <div class="media align-items-center">
                            <div class="img-thumbnail rounded-circle position-relative" style="width: 6.375rem;"><span class="badge badge-warning" data-toggle="tooltip" title="Reward points">384</span><img class="rounded-circle" src="{{asset('asset/frontend/img/amit.jpg')}}" alt="Amit Saha"></div>
                            <div class="media-body pl-3">
                                <h3 class="font-size-base mb-0">Amit Saha</h3><span class="text-accent font-size-sm">amitsaha5bd@gmail.com</span>
                            </div>
                        </div>
                    </div>
                    <div class="bg-secondary px-4 py-3">
                        <h3 class="font-size-sm mb-0 text-muted">Account settings</h3>
                    </div>
                    <ul class="list-unstyled mb-0">
                        <li class="border-bottom mb-0"><a class="nav-link-style d-flex align-items-center px-4 py-3 active" href="{{url('/userAccount')}}"><i class="czi-basket opacity-60 mr-2"></i>Dashboard</a></li>
                        <li class="border-bottom mb-0"><a class="nav-link-style d-flex align-items-center px-4 py-3 " href="{{url('/userProfile')}}"><i class="czi-user opacity-60 mr-2"></i>Profile info</a></li>
                        <li class="border-bottom mb-0"><a class="nav-link-style d-flex align-items-center px-4 py-3" href="{{url('/userPassword')}}"><i class="czi-location opacity-60 mr-2"></i>Passwords</a></li>
                        <li class="mb-0"><a class="nav-link-style d-flex align-items-center px-4 py-3" href="{{url('/userPayment')}}"><i class="czi-card opacity-60 mr-2"></i>Payment methods</a></li>
                    </ul>
                </div>
            </aside>
            <!-- Content  -->
            <section class="col-lg-8" >
                <!-- Toolbar-->
                <div class="d-none d-lg-flex justify-content-between align-items-center pt-lg-3 pb-4 pb-lg-5 mb-lg-3">
                    <h6 class="font-size-base text-light mb-0">Account Dashboard:</h6><a class="btn btn-primary btn-sm" href="{{'/logout'}}"><i class="czi-sign-out mr-2"></i>Sign out</a>
                </div>
                <!-- Profile form-->
                <div  >
                    <div class="bg-primary rounded-lg p-4 mb-4" style="background-color: #FE4344;" >
                        <div class=" text-light align-items-center">
                            <h1 class="text-light shadow text-shadow text-center mt-5 mb-5">Hi, <br> Amit Saha. </h1>
                            <br>
                            <hr>
                            <h3 class=" text-light  text-center mt-5 mb-5 ">Welcome To Your Account.</h3>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@stop
