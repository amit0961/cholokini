@extends('layouts.frontend.frontendDesign')
@section('content')
    <div class="page-title-overlap bg-dark pt-4">
        <div class="container d-lg-flex justify-content-between py-2 py-lg-3">
            <div class="order-lg-2 mb-3 mb-lg-0 pt-lg-2">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-light flex-lg-nowrap justify-content-center justify-content-lg-star">
                        <li class="breadcrumb-item"><a class="text-nowrap" href="index.html"><i class="czi-home"></i>Home</a></li>
                        <li class="breadcrumb-item text-nowrap"><a href="#">Account</a>
                        </li>
                        <li class="breadcrumb-item text-nowrap active" aria-current="page">Profile</li>
                    </ol>
                </nav>
            </div>
            <div class="order-lg-1 pr-lg-4 text-center text-lg-left">
                <h1 class="h3 text-light mb-0">Profile</h1>
            </div>
        </div>
    </div>
    <!-- Page Content-->
    <div class="container pb-5 mb-2 mb-md-3">
        <div class="row">
            <!-- Sidebar-->
            <aside class="col-lg-4 pt-4 pt-lg-0">
                <div class="cz-sidebar-static rounded-lg box-shadow-lg px-0 pb-0 mb-5 mb-lg-0">
                    <div class="px-4 mb-4">
                        <div class="media align-items-center">
                            <div class="img-thumbnail rounded-circle position-relative" style="width: 6.375rem;"><span class="badge badge-warning" data-toggle="tooltip" title="Reward points"></span><img class="rounded-circle" src="{{asset('asset/frontend/img/amit.jpeg')}}" alt="Amit Saha"></div>
                            <div class="media-body pl-3">
                                <h3 class="font-size-base mb-0  shadow "> <i class="czi-user"></i> {{$userDetails->name}} </h3>
                                <hr>
                                <span class="text-accent font-size-sm"><i class="czi-mail"></i> {{$userDetails->email}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="bg-secondary px-4 py-3">
                        <h3 class="font-size-sm mb-0 text-muted">Account settings</h3>
                    </div>
                    <ul class="list-unstyled mb-0">
                        <li class="border-bottom mb-0"><a class="nav-link-style d-flex align-items-center px-4 py-3 active" href="{{url('/userAccount')}}"><i class="czi-basket opacity-60 mr-2"></i>Dashboard</a></li>
                        <li class="border-bottom mb-0"><a class="nav-link-style d-flex align-items-center px-4 py-3 " href="{{url('/userProfile')}}"><i class="czi-user opacity-60 mr-2"></i>Profile info</a></li>
                        <li class="border-bottom mb-0"><a class="nav-link-style d-flex align-items-center px-4 py-3" href="{{url('/userPassword')}}"><i class="czi-location opacity-60 mr-2"></i>Password</a></li>
                        <li class="mb-0"><a class="nav-link-style d-flex align-items-center px-4 py-3" href="{{url('/userPayment')}}"><i class="czi-card opacity-60 mr-2"></i>Payment methods</a></li>
                    </ul>
                </div>
            </aside>
            <!-- Content  -->
            <section class="col-lg-8" >
                <!-- Toolbar-->
                <div class="d-none d-lg-flex justify-content-between align-items-center pt-lg-3 pb-4 pb-lg-5 mb-lg-3">
                    <h6 class="font-size-base text-light mb-0">Update you profile details below:</h6><a class="btn btn-primary btn-sm" href="{{'/logout'}}"><i class="czi-sign-out mr-2"></i>Sign out</a>
                </div>
                <!-- Profile form-->
                @if (Session::get('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong><span class="border-bottom shadow">{!! session('success') !!}</span></strong>
                    </div>
                @endif
                <form id="profileForm" name="profileForm" method="POST" action="{{url('/userProfile')}}">
                    {{csrf_field()}}
                    <div class="row shadow">
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label for="name"><span class="h6 border-bottom shadow" >Name: </span></label>
                                <input class="form-control" type="text" name="name" id="name" value="{{$userDetails->name}}" placeholder="Enter your name here..." required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="address1"><span class="h6 border-bottom shadow" >Address:1: </span></label>
                                <input class="form-control" type="text" name="address1" id="address1" value="{{$userDetails->address1}}" placeholder="Enter Your Address ...." required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="account-ln"><span class="h6 border-bottom shadow" >Address:2:  </span>(not important)</label>
                                <input class="form-control" type="text" name="address2" id="address2" value="{{$userDetails->address2}}" placeholder="Enter Your Address ....">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="division"><span class="h6 border-bottom shadow" >Divisions: </span></label>
                                <select class="custom-select" name="division" id="division" required>
                                    <option value="" disabled selected>Select Division</option>
                                    @foreach($divisions as $division)
                                        <option value="{{$division->bn_name}}" @if($division->bn_name == $userDetails->division) selected @endif >{{$division->bn_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="district"><span class="h6 border-bottom shadow" >Districts: </span></label>
                                <select class="custom-select" name="district" id="district" required>
                                    <option value="" disabled selected>Select District</option>
                                    @foreach($districts as $district)
                                        <option value="{{$district->bn_name}}" @if($district->bn_name == $userDetails->district) selected @endif >{{$district->bn_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="upazila"><span class="h6 border-bottom shadow" >Upazila: </span></label>
                                <select class="custom-select" name="upazila" id="upazila" required>
                                    <option value="" disabled selected>Select Upazila</option>
                                    @foreach($upazilas as $upazila)
                                        <option value="{{$upazila->bn_name}}" @if($upazila->bn_name == $userDetails->upazila) selected @endif >{{$upazila->bn_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="union"><span class="h6 border-bottom shadow" >Unions: </span></label>
                                <select class="custom-select" name="union" id="union" required>
                                    <option value="" disabled selected>Select Union</option>
                                    @foreach($unions as $union)
                                        <option value="{{$union->bn_name}}" @if($union->bn_name == $userDetails->union) selected @endif >{{$union->bn_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>



                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="pincode"><span class="h6 border-bottom shadow" >Pin-Code: </span></label>
                                <input class="form-control" type="text" name="pincode" id="pincode" value="{{$userDetails->pincode}}" placeholder="Submit your pincode" required>
                            </div>
                        </div>
                        <div class="col-sm-6 mb-3">
                            <div class="form-group">
                                <label for="phone"><span class="h6 border-bottom shadow" >Phone Number: </span></label>
                                <input class="form-control" type="text" name="mobile" id="mobile" value="{{$userDetails->mobile}}" placeholder="Your Phone No please" required>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="d-flex flex-wrap justify-content-between align-items-center">
                                <div class="custom-control custom-checkbox d-block">

                                </div>
                                <button class="btn btn-primary mt-3 mt-sm-0 mb-5 " type="submit">Update profile</button>
                            </div>
                        </div>
                    </div>
                </form>
            </section>
        </div>
    </div>
@stop
